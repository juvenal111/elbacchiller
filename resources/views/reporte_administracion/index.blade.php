@extends('voyager::master')

@section('page_title', 'Reporte para el admiistrador')

@section('page_header')
@stop

@section('content')
    <div class="row">
        <a href="{{route('promotor.index')}}">
            <div class="col-md-4" style="background: red">Promotores</div>
        </a>
        <a href="{{route('user.index')}}">
            <div class="col-md-4" style="background: green">Usuarios</div>
        </a>
        <a href="{{route('reporte.ventas')}}">
            <div class="col-md-4" style="background: #ff9c00">Ventas</div>
        </a>

    </div>
@stop

@section('css')
@stop

@section('javascript')
    <script>
        $(document).ready(
            function () {

            }
        );
    </script>
@stop
