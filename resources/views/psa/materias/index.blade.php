@extends('voyager::master')

@section('content')

<div class="container">
  <div class="panel panel-default">
        
              <ol class="breadcrumb">
                  <li><a href="#">Inicio</a></li>
                  <li><a href="#">PSA Areas</a></li>
                  <li class="active">PSA Area-materias</li>
              </ol>   
         
       <div class="row">
         <div class="col-md-12">

<!--               <h3>Gestion: {{$area->nombre_gestion}}</h3> -->
           
              <h2 class="text-center">Materias del {{$area->nombre}}</h2>
         </div>

      </div>
  
      <div class="row">
        <div class="panel-body">
            <div class="table-responsive">
               <table class="table table-hover">
                 <thead>
<!--                     <th>id</th> -->
                    <th>materia</th>
                    <th>acciones</th>
                 </thead>             
                 <tbody>
                   @foreach($materias as $materia)
                      <tr>
<!--                         <td>{{$materia->id}}</td> -->
                        <td>{{$materia->nombre}}</td>
                        <td> 
                          <a class="btn btn-info" href="{{route('ver-preguntas',['area_id' =>$area->id,'materia_id'=>$materia->id] )}}"  
                             target="_blank">ver preguntas</a>
                        </td>
                      </tr> 
                   @endforeach
                 </tbody>
              </table>  
            </div>
          {{ $materias->links() }}
        </div>
      </div>
 
  </div>  

</div>

@endsection