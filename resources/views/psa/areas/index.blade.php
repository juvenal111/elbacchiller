@extends('voyager::master')

@section('content')

<div class="container">
  <div class="panel panel-default">
     <div class="row">
        <div class="col-md-4">
          <h1>AREAS DEL PSA</h1>
            
        </div>
        <div class="col-md-4">
               <form class="form-inline" >
                   <input class="form-control" name="search" type="search" placeholder="escribe un nombre...">
                   <button class="btn btn-info" type="submit">Buscar</button>
                </form>
        </div>
              <div class="col-md-4">
                 <button class="btn btn-success" href="#">nuevo</button>
              </div>
      </div>
  
  <div class="row">
    <div class="panel-body">
      
        <div class="table-responsive">
           <table class="table table-striped">
             
             <thead>
                <th>id</th>
                <th>nombre</th>
                <th>acciones</th>
             </thead>
             
             <tbody>
               @foreach($gestiones as $gestion)
                  <tr>
                    <td>{{$gestion->id}}</td>
                    <td>{{$gestion->nombre}}</td>
                    <td> 
<!--                       <a class="btn btn-primary" href="{{route('ver-areas',$gestion->id)}}">ver materias</a> -->
                      <button class="btn btn-warning" href="#">editar</button>
                      <button class="btn btn-danger" href="#">borrar</button>
                    </td>
                  </tr> 
               @endforeach
             </tbody>
             
          </table>  
        </div>

    </div>
  </div>
  
  </div>  
  

</div>


@endsection