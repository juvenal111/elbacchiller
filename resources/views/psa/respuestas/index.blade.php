@extends('voyager::master')

@section('content')

<div class="container">
  <div class="panel panel-default">
              <ol class="breadcrumb">
                  <li><a href="#">Inicio</a></li>
                  <li><a href="#">PSA Areas</a></li>
                  <li><a href="#">PSA Area-materias</a></li>
                  <li><a href="#">PSA Pregunta</a></li>
                  <li class="active">PSA Respuesta</li>
               </ol>
    
<!--       <h3 class="text-center"> {{$pregunta->texto}}</h3>   -->
         <div class="row">
           <div class="col-md-12"> 
                <h3 class="text-center">{{$pregunta->texto}} </h3>
           </div>
        </div>      
    
  <div class="panel-body">
    

    <div class="row bg-warning">
       <div class="col-md-8">
                <form class="form-horizontal" action="{{route('editar-pregunta',$pregunta->id)}}" method="POST" enctype="multipart/form-data">
                       {{ csrf_field() }}
                  <br>
                    <div class="form-group">
                      <label class="control-label col-sm-2" for="email">id:</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" value="{{$pregunta->id}}" name="email" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-sm-2" for="pwd">Texto:</label>
                      <div class="col-sm-10">          
                        <input type="text" class="form-control" id="texto" value="{{$pregunta->texto}}" name="texto" >
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="control-label col-sm-2" for="imagen">Imagen:</label>
                      <div class="col-sm-10">          
                        <input type="file" id="imagen" name="imagen">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-sm-2" for="pwd">Texto 2:</label>
                      <div class="col-sm-10">          
                        <input type="text" class="form-control" id="texto2" value="{{$pregunta->texto2}}" name="texto2" >
                      </div>
                    </div>
                    <div class="form-group">        
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-warning pull-right">Guardar</button>
                      </div>
                    </div>
              </form>
      
      </div>
      <div class="col-md-2">
        <br>
        <img src="http://admin.el-bachiller.com/storage/{{$pregunta->imagen}}" alt="imagen" width="250" height="250">
       
      </div>
    </div>
    


        <div class="row">
                  <div class="col-md-4">
                      <h3>Respuestas:</h3>
                  </div>
                  <div class="col-md-6">
                  </div>
                  <div class="col-md-2">
                          <!-- Button trigger modal -->
                            <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#myModal">
                              nueva respuesta
                            </button>
                  </div>
          
          
          
        </div>
      <div class='row'>
       
       
      
      
      
      
      
      
      
      
      
        <div class="table-responsive">
           <table class="table table-hover table-condensed">
             
             <thead>
                <th>texto</th>
                <th>imagen</th>
                <th>tipo</th>
                <th>acciones</th>
             </thead>
             
             <tbody>
               @foreach($respuestas as $respuesta)
                  <tr>
                    <td>{{$respuesta->texto}}</td>
                    <td>
                        <img src="http://admin.el-bachiller.com/storage/{{$respuesta->imagen}}" alt="imagen" width="50" height="50">                 
                    </td>
                    <td>
                      @if($respuesta->tipo == 'correcta')
                       <span class="label label-success">{{$respuesta->tipo}}</span>
                      @else
                        <span class="label label-danger">{{$respuesta->tipo}}</span>
                      @endif
                    </td>
                    <td> 

                       <!-- Button trigger modal -->
                       <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit{{$respuesta->id}}" >
                              
                         <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>

                       </button>
                      
                      <!-- Button trigger modal -->
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminar{{$respuesta->id}}">
                                   <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                            </button>                   
                      
                                    <!-- Modal -->
                                    <div class="modal fade" id="edit{{$respuesta->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title text-center" id="exampleModalLabel">Editar respuesta</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <form method="POST" action="{{route('editar-respuesta',$respuesta->id)}}" enctype="multipart/form-data">
                                                           {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <label for="texto">Texto</label>
                                                            <textarea class="form-control" rows="3"  id="texto"  name="texto" required>{{$respuesta->texto}}</textarea>
<!--                                                             <input type="text" class="form-control" id="texto"  name="texto" value="{{$respuesta->texto}}" required> -->
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="imagen">imagen</label>
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" id="imagen" name="imagen"/>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="tipo">Tipo</label>
                                                            <select class="form-control" id="tipo" name="tipo">
                                                              @if($respuesta->tipo == 'incorrecta')
                                                                <option value="incorrecta">incorrecta</option>
                                                                <option value="correcta">correcta</option>
                                                              @else
                                                                <option value="correcta">correcta</option>
                                                                <option value="incorrecta">incorrecta</option>
                                                              @endif
                                                            </select>
                                                        </div>


                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                            <button type="submit" class="btn btn-primary" id="but">Guardar</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                      
                      
                      
                                                        <!-- Modal -->
                                    <div class="modal fade" id="eliminar{{$respuesta->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Eliminar respuesta </h5>
                                                </div>
                                                <div class="modal-body">
                                                  <p>Esta seguro que quiere eliminar esta respuesta?</p>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                             <a class="btn btn-danger" href="{{route('eliminar-respuesta',$respuesta->id)}}">Si</span></a>

                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
  
                    </td>
                  </tr> 
               @endforeach
             </tbody>
             
          </table>  
        </div>

    </div>
  </div>
  
  </div>  
  

</div>















<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center" id="exampleModalLabel">Nueva respuesta</h4>
            </div>
            <div class="modal-body">

                <form method="POST" action="{{route('guardar-respuesta',$pregunta->id)}}" enctype="multipart/form-data">
                       {{ csrf_field() }}
                    <div class="form-group">
                        <label for="texto">Texto</label>
<!--                         <input type="text" class="form-control" id="texto"  name="texto" placeholder="..." required> -->
                             <textarea class="form-control" rows="3"  id="texto"  name="texto" required></textarea>
                    </div>

                    <div class="form-group">
                        <label for="imagen">imagen</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="imagen" name="imagen"/>
                        </div>
                    </div>
                  
                    <div class="form-group">
                        <label for="tipo">Tipo</label>
                        <select class="form-control" id="tipo" name="tipo">
                          <option value="incorrecta">incorrecta</option>
                          <option value="correcta">correcta</option>
                        </select>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="but">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>













@endsection