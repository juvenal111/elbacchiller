@extends('voyager::master')

@section('content')

<div class="container">
  
  <div class="panel panel-default">
               <ol class="breadcrumb">
                  <li><a href="#">Inicio</a></li>
                  <li><a href="#">PSA Areas</a></li>
                  <li><a href="#">PSA Area-materias</a></li>
                  <li class="active">PSA Materia</li>
               </ol>
   
         <div class="row">
           <div class="col-md-12"> 
                <h2 class="text-center"> {{$materia->nombre}} </h2>
           </div>
        </div>  
    
    
    
    
  <div class="panel-body">
    
<!--     <h2 class="text-center"> {{$materia->nombre}} </h2> -->
    <div class="row  bg-warning">
         <div class="col-md-8">
                  <form class="form-horizontal" action="{{route('editar-area-materia',$area_materia->id)}}" method="POST">
                         {{ csrf_field() }}
                      <br>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Gestion:</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="email" value="{{$area->nombre_gestion}}" name="email" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Area:</label>
                        <div class="col-sm-10">          
                          <input type="text" class="form-control" id="pwd" value="{{$area->nombre}}" name="pwd" readonly>
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-sm-2" for="link_texto">Link de texto:</label>
                        <div class="col-sm-10">          
                          <input type="text" class="form-control" id="link_texto" name="link_texto" value="{{$area_materia->link_texto}}">
                        </div>
                      </div>
                      <div class="form-group">        
                        <div class="col-sm-offset-2 col-sm-10">
                          @if($area_materia->link_texto)
                             <a href="{{$area_materia->link_texto}}" class="btn btn-info"  target="_blank"> ir al link</a> 
                          @endif
                             
                          <button type="submit" class="btn btn-warning pull-right">Guardar</button>
                        </div>
                      </div>
                </form>
        </div>
    </div>
    
    <div class="row">
          <div class="col-md-4">
              <h3>Preguntas:</h3>
          </div>
           <div class="col-md-6">
               <form class="form-inline" >
                             <input class="form-control" name="search" type="search" placeholder="escribe un id...">
                             <button class="btn btn-default" type="submit">Buscar</button>
                </form>
          </div>
           <div class="col-md-2">
                  <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#myModal">
                        nueva pregunta
                     </button>
          </div>        
     </div>
      <div class='row'>
       
        <div class="table-responsive">
           <table class="table table-hover table-condensed">
             
             <thead>
                <th>id</th>
                <th>texto</th>
                <th>imagen</th>
                <th>texto2</th>
                <th>acciones</th>
             </thead>
             
             <tbody>
               @foreach($preguntas as $pregunta)
                  <tr>
                    <td>{{$pregunta->id}}</td>
                    <td>{{$pregunta->texto}}</td>
                    <td>
                        <img src="http://admin.el-bachiller.com/storage/{{$pregunta->imagen}}" alt="imagen" width="50" height="50">
                    </td>
                    <td>{{$pregunta->texto2}}</td>
                    <td> 
                        <a class="btn btn-info" href="{{route('ver-respuestas',$pregunta->id )}}"  target="_blank">ver</a>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminar{{$pregunta->id}}">
                                   <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </button>
                                    <!-- Modal -->
                                   <div class="modal fade" id="eliminar{{$pregunta->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Eliminar pregunta </h5>
                                                </div>
                                                <div class="modal-body">
                                                  <p>Esta seguro que quiere eliminar esta pregunta con todas sus respuesta?</p>
                                                   <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                             <a class="btn btn-danger" href="{{route('eliminar-pregunta',$pregunta->id)}}">Si</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                    </td>
                  </tr> 
               @endforeach
             </tbody>
             
          </table>  
        </div>
      {{ $preguntas->links() }}
    </div>
  </div>
  
  </div>  
  
</div>















<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center" id="exampleModalLabel">Nueva pregunta</h4>
            </div>
            <div class="modal-body">

                <form method="POST" action="{{route('guardar-pregunta',$area_materia->id) }}" enctype="multipart/form-data">
                       {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Texto</label>
<!--                         <input type="text" class="form-control" id="texto"  name="texto" required> -->
                            <textarea class="form-control" rows="3"  id="texto"  name="texto" required></textarea>
                    </div>


                    <div class="form-group">
                        <label for="photo">Imagen (opcional)</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="imagen" name="imagen"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Texto 2 (opcional)</label>
                        <input type="text" class="form-control" id="texto2"  name="texto2" >
                    </div>         

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="but">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>










@endsection