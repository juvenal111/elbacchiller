@extends('voyager::master')

@section('content')

    <div class="container">
            <div class="row">
                <h1>Puntos de compra en {{$ciudad->nombre}}</h1>
            </div>
            <div class="row">
                <div class="panel-body">
                    <div class="form-group col-md-12">
                        <div class="col-sm-10">
                            <div style="width: 100%; height: 600px;">
                                {!! Mapper::render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>

    <script>
        function onMapLoad(map,latt,lngg,puntos) {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    function(position) {
                        for (var i = 0; i < puntos.length; i+=1) {
                            var pos = {
                                lat: parseFloat(puntos[i].lat),
                                lng: parseFloat(puntos[i].lng)
                            };
                            var marker = new google.maps.Marker({
                                position: pos,
                                map: map,
                                draggable:false,
                                title: puntos[i].nombre
                            });
                        }
                        map.setCenter(pos);
                    }
                );
            }
        }
    </script>
@endsection