@extends('voyager::master')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                    <h1>Detalle de la Venta  {{$venta->id}}</h1>


                <div class="panel-body">

                    <p>Fecha:   {{$venta->fecha}}</p>
                    <p>Cliente: {{$venta->cliente}}</p>
                    <div class="table-responsive">
                        <table class="table table-striped">

                            <thead>
                            <th>producto</th>
                            <th>precio</th>
                            <th>cantidad</th>
                            <th>subtotal</th>
                            </thead>

                            <tbody>
                            @foreach($detalles as $detalle)
                                <tr>
                                    <td>{{$detalle->producto}}</td>
                                    <td>{{$detalle->precio_actual}}</td>
                                    <td>{{$detalle->cantidad}}</td>
                                    <td>{{$detalle->subtotal}}</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>

                    <h1>Total:  Bs. {{$venta->total}}</h1>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection