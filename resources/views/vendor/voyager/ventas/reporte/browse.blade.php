@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-bug"></i> {{ $dataType->display_name_plural }}
        </h1>
     </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">

                        <div class="table-responsive" id="mostrarTabla">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <td>
                                        <select class="form-control select2" id="pais_venta">
                                            @foreach($paises as $pais)
                                            <option value="{{$pais->id}}">{{ $pais->nombre }}</option>
                                            @endforeach
                                        </select>

                                    </td>
                                    <td>
                                        <select class="form-control select2" id="departamento_venta">
                                            <option value="0" selected>{{ 'todos' }}</option>
                                        </select>

                                    </td>
                                    <td>
                                        <input type="button" onclick="printDiv('mostrarTabla')" value="imprimir div" />
                                    </td>

                                </tr>
                                    <tr>
                                            <th>
                                                Total
                                            </th>
                                        <th>
                                            Pais
                                        </th>
                                        <th>
                                            Departamento
                                        </th>

                                    </tr>
                                </thead>
                                <tbody id="body_reporte">

                                        @foreach($dataTypeContent as $row)
                                        <tr>
                                            <td>
                                                {{$row->total}}
                                            </td>
                                            <td>
                                                {{$row->pais}}

                                            </td>
                                            <td>
                                                {{$row->departamento}}

                                            </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop


@section('javascript')
    <!-- DataTables -->
    <script>
        var params = {}
        $(document).ready(function () {
            $("#body_reporte").empty()
            var pais_id= $('#pais_venta').val()
            devolverDepartamentosVentas(pais_id)
            devolverReporteVentas(pais_id)

            //alert(id)
        });
        $('#pais_venta').on('change', function(){
            var pais_id= $('#pais_venta').val()
            $("#departamento_venta").empty()
            devolverDepartamentosVentas(pais_id)
            devolverReporteVentas(pais_id)
        });
        $('#departamento_venta').on('change', function(){
            var departamento_id= $('#departamento_venta').val()
            $("#body_reporte").empty()
            devolverReporteVentasDepartamento(departamento_id)
        });

        function printDiv(nombreDiv) {
            var contenido= document.getElementById(nombreDiv).innerHTML;
            var contenidoOriginal= document.body.innerHTML;

            document.body.innerHTML = contenido;

            window.print();

            document.body.innerHTML = contenidoOriginal;
        }


        function devolverReporteVentasDepartamento(departamento_id){
            params = {
                departamento_id:     departamento_id,
                _token: '{{ csrf_token() }}'
            }
            $.post("{{route('get_ventas_departamento')}}",params,  function (response) {
                if ( response
                    && response.data
                    && response.data.status
                    && response.data.status == 200 ) {
                    $("#body_reporte").empty()
                    for (var i = 0; i < response.data.data.length; i++) {
                        $("#dataTable").
                            append('<tr><td>'+response.data.data[i].total+'</td>' +
                                '<td>'+response.data.data[i].pais+'</td>'+
                                '<td>'+response.data.data[i].departamento+'</td>'
                                +'</tr>');
                    }
                    console.log(response.data.data)
                    //toastr.success(response.data.message);
                    //$image.parent().fadeOut(300, function() { $(this).remove(); })
                } else {
                    //toastr.error("Error removing image.");
                }



//                if ( response1) {
////                    $("#departamento_venta").
////                        append('<option value="0" selected>{{ 'todos' }}</option>')
////
////                    for (var i = 0; i < response1.data.data.length; i++) {
////                        $("#departamento_venta").
////                            append('<option value="'+ response1.data.data[i].id +'">'+ response1.data.data[i].nombre +'</option>');
////                    }
//                    console.log(response1);
//                } else {
//                    console.log(response1);
//                    //Materialize.toast('Error', 3200);
//                }
            });
        }


        function devolverReporteVentas(pais_id){
            params = {
                pais_id:     pais_id,
                _token: '{{ csrf_token() }}'
            }
            $.post("{{route('get_ventas')}}",params,  function (response) {
                if ( response
                    && response.data
                    && response.data.status
                    && response.data.status == 200 ) {
                    $("#body_reporte").empty()
                    for (var i = 0; i < response.data.data.length; i++) {
                        $("#dataTable").
                            append('<tr><td>'+response.data.data[i].total+'</td>' +
                                '<td>'+response.data.data[i].pais+'</td>'+
                                '<td>'+response.data.data[i].departamento+'</td>'
                                +'</tr>');
                    }
                    console.log(response.data.data)
                    //toastr.success(response.data.message);
                    //$image.parent().fadeOut(300, function() { $(this).remove(); })
                } else {
                    //toastr.error("Error removing image.");
                }



//                if ( response1) {
////                    $("#departamento_venta").
////                        append('<option value="0" selected>{{ 'todos' }}</option>')
////
////                    for (var i = 0; i < response1.data.data.length; i++) {
////                        $("#departamento_venta").
////                            append('<option value="'+ response1.data.data[i].id +'">'+ response1.data.data[i].nombre +'</option>');
////                    }
//                    console.log(response1);
//                } else {
//                    console.log(response1);
//                    //Materialize.toast('Error', 3200);
//                }
            });
        }

        function devolverDepartamentosVentas(pais_id){
            params = {
                pais_id:     pais_id,
            }
            $.get("{{route('get_departamentos')}}",params,  function (response1) {
                if ( response1) {
                    $("#departamento_venta").
                        append('<option value="0" selected>{{ 'todos' }}</option>')

                    for (var i = 0; i < response1.data.data.length; i++) {
                        $("#departamento_venta").
                            append('<option value="'+ response1.data.data[i].id +'">'+ response1.data.data[i].nombre +'</option>');
                    }
                    //console.log(response1.data.data);
                } else {
                    //console.log(response1);
                    //Materialize.toast('Error', 3200);
                }
            });
        }

    </script>
@stop
