@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ 'Viendo promotor' }} &nbsp;

        {{--@can('edit', $dataTypeContent)--}}
        {{--<a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">--}}
            {{--<span class="glyphicon glyphicon-pencil"></span>&nbsp;--}}
            {{--{{ __('voyager::generic.edit') }}--}}
        {{--</a>--}}
        {{--@endcan--}}
        {{--@can('delete', $dataTypeContent)--}}
            {{--<a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger delete" data-id="{{ $dataTypeContent->getKey() }}" id="delete-{{ $dataTypeContent->getKey() }}">--}}
                {{--<i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>--}}
            {{--</a>--}}
        {{--@endcan--}}

        {{--<a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">--}}
            {{--<span class="glyphicon glyphicon-list"></span>&nbsp;--}}
            {{--{{ __('voyager::generic.return_to_list') }}--}}
        {{--</a>--}}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered" style="padding-bottom:5px;">
                    <!-- form start -->

                    @php
                        //dd($dataType->readRows);
                            $row_id_aplicacion=array_pull($dataType->readRows, '1');
                            array_forget($dataType->readRows, '2');
                            array_forget($dataType->readRows, '4');
                            array_forget($dataType->readRows, '5');
                            array_forget($dataType->readRows, '6');
                            array_forget($dataType->readRows, '7');
                            array_forget($dataType->readRows, '8');
                            array_forget($dataType->readRows, '10');
                            array_forget($dataType->readRows, '13');
                            array_forget($dataType->readRows, '14');
                            array_forget($dataType->readRows, '15');
                            array_forget($dataType->readRows, '16');
                            array_forget($dataType->readRows, '17');
                            array_forget($dataType->readRows, '18');
                            array_forget($dataType->readRows, '12');
                            array_forget($dataType->readRows, '19');
                            array_forget($dataType->readRows, '22');
                            array_forget($dataType->readRows, '23');
                            $codigos_enviados=array_pull($dataType->readRows, '24');
                            $fecha_envio=array_pull($dataType->readRows, '28');
                            array_forget($dataType->readRows, '29');
                            array_forget($dataType->readRows, '30');
                            $row25=$dataType->readRows['25'];
                            $dataType->readRows['25']=$dataType->readRows['26'];
                            $dataType->readRows['26']=$row25;
                            array_add($dataType->readRows, 2, $row_id_aplicacion);
                            //31,33,35,36,2
                            $row31=$dataType->readRows['31'];
                            $row33=$dataType->readRows['33'];
                            $row35=$dataType->readRows['35'];
                            $row36=$dataType->readRows['36'];
                            $row2=$dataType->readRows['2'];
                            $dataType->readRows['31']=$row2;

                            $dataType->readRows['33']=$row31;
                            $dataType->readRows['35']=$row33;
                            $dataType->readRows['36']=$row35;
                            $dataType->readRows['2']=$row36;
                            array_add($dataType->readRows, 28, $fecha_envio);
                            $dataType->readRows['28']->field='fecha_inscripcion';
                            $dataType->readRows['28']->display_name='Fecha inscripcion';
                            $dataType->readRows['28']->save();
                            $codigos_sin_vender =  clone ($codigos_enviados);
                            $historial_codigos =  clone ($codigos_enviados);
                            array_add($dataType->readRows, 4, $codigos_enviados);
                            array_add($dataType->readRows, 5, $codigos_sin_vender);

                            $dataType->readRows['4']->field='total_codigos_vendidos';
                            $dataType->readRows['4']->display_name='Total codigos vendidos';
                            $dataType->readRows['4']->type='number';
                            $dataType->readRows['4']->save();

                            $dataType->readRows['5']->field='total_codigos_sin_vender';
                            $dataType->readRows['5']->display_name='Total codigos sin vender';
                            $dataType->readRows['5']->type='number';
                            $dataType->readRows['5']->save();

                            array_add($dataType->readRows, 6, $historial_codigos);

                            $dataType->readRows['6']->field='historial_codigos';
                            $dataType->readRows['6']->display_name='Historial de codigos';
                            $dataType->readRows['6']->type='text';
                            $dataType->readRows['6']->save();
                            //dd($dataType->readRows);

                    @endphp

                    @foreach($dataType->readRows as $row)
                        @php $rowDetails = json_decode($row->details);
                         if($rowDetails === null){
                                $rowDetails=new stdClass();
                                $rowDetails->options=new stdClass();
                         }
                        @endphp

                        <div class="form-group  col-md-12">
                            <label for="name">{{ $row->display_name }}</label>

                            <div class="panel-body" style="padding-top:0;">
                                @if($row->type == "image")
                                    <img class="img-responsive"
                                         src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                                @elseif($row->type == 'multiple_images')
                                    @if(json_decode($dataTypeContent->{$row->field}))
                                        @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                            <img class="img-responsive"
                                                 src="{{ filter_var($file, FILTER_VALIDATE_URL) ? $file : Voyager::image($file) }}">
                                        @endforeach
                                    @else
                                        <img class="img-responsive"
                                             src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                                    @endif
                                @elseif($row->type == 'relationship')
                                     @include('voyager::formfields.relationship', ['view' => 'read', 'options' => $rowDetails])
                                @elseif($row->type == 'select_dropdown' && property_exists($rowDetails, 'options') &&
                                        !empty($rowDetails->options->{$dataTypeContent->{$row->field}})
                                )

                                    <?php echo $rowDetails->options->{$dataTypeContent->{$row->field}};?>
                                @elseif($row->type == 'select_dropdown' && $dataTypeContent->{$row->field . '_page_slug'})
                                    <a href="{{ $dataTypeContent->{$row->field . '_page_slug'} }}">{{ $dataTypeContent->{$row->field}  }}</a>
                                @elseif($row->type == 'select_multiple')
                                    @if(property_exists($rowDetails, 'relationship'))

                                        @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                            @if($item->{$row->field . '_page_slug'})
                                            <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field}  }}</a>@if(!$loop->last), @endif
                                            @else
                                            {{ $item->{$row->field}  }}
                                            @endif
                                        @endforeach

                                    @elseif(property_exists($rowDetails, 'options'))
                                        @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                         {{ $rowDetails->options->{$item} . (!$loop->last ? ', ' : '') }}
                                        @endforeach
                                    @endif
                                @elseif($row->type == 'date' || $row->type == 'timestamp')
                                    {{ $rowDetails && property_exists($rowDetails, 'format') ? \Carbon\Carbon::parse($dataTypeContent->{$row->field})->formatLocalized($rowDetails->format) : $dataTypeContent->{$row->field} }}
                                @elseif($row->type == 'checkbox')
                                    @if($rowDetails && property_exists($rowDetails, 'on') && property_exists($rowDetails, 'off'))
                                        @if($dataTypeContent->{$row->field})
                                        <span class="label label-info">{{ $rowDetails->on }}</span>
                                        @else
                                        <span class="label label-primary">{{ $rowDetails->off }}</span>
                                        @endif
                                    @else
                                    {{ $dataTypeContent->{$row->field} }}
                                    @endif
                                @elseif($row->type == 'color')
                                    <span class="badge badge-lg" style="background-color: {{ $dataTypeContent->{$row->field} }}">{{ $dataTypeContent->{$row->field} }}</span>
                                @elseif($row->type == 'coordinates')
                                    @include('voyager::partials.coordinates')
                                @elseif($row->type == 'rich_text_box')
                                    @include('voyager::multilingual.input-hidden-bread-read')
                                    <p>{!! $dataTypeContent->{$row->field} !!}</p>
                                @elseif($row->type == 'file')
                                    @if(json_decode($dataTypeContent->{$row->field}))
                                        @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}">
                                                {{ $file->original_name ?: '' }}
                                            </a>
                                            <br/>
                                        @endforeach
                                    @else
                                        <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($row->field) ?: '' }}">
                                            {{ __('voyager::generic.download') }}
                                        </a>
                                    @endif
                                @else
                                    @if($row->field=='total_codigos_vendidos')
                                        @php
                                            $total_vendido=\Illuminate\Support\Facades\DB::select("
                                                select count(*) as codigos_vendidos
                                                from activaciones as a
                                                where a.emisor_id=$dataTypeContent->id
                                            ");
                                            //dd($total_vendido['0']->codigos_vendidos);
                                            $mostrar=$total_vendido['0']->codigos_vendidos;

                                        @endphp
                                        @include('voyager::multilingual.input-hidden-bread-read')
                                        <p>{{ $mostrar }}</p>
                                    @elseif($row->field=='total_codigos_sin_vender')
                                        @php
                                            $array_total_comprado=\Illuminate\Support\Facades\DB::select("
                                                select sum(dv.cantidad) as cantidad, v.cliente_id
                                                from ventas as v, detalle_ventas as dv
                                                where v.id=dv.venta_id and v.cliente_id=$dataTypeContent->id
                                                group by cantidad, v.cliente_id
                                            ");
                                            //dd($array_total_comprado);
                                            $suma=0;
                                            foreach($array_total_comprado as $row){
                                                $suma= $suma + $row->cantidad;
                                            }
                                           $total_comprado=$suma;
                                            $mostrar=$total_comprado-$total_vendido['0']->codigos_vendidos;
                                    //echo $dataTypeContent->id;

                                        @endphp
                                        <div class="row">
                                            <div class="col-sm-6 col-lg-6 col-md-6 col-xs-6">
                                                <p id="codigo_sin_vender">{{ $mostrar }}</p>
                                            </div>
                                            {{--<div class="col-sm-6 col-lg-6 col-md-6 col-xs-6">--}}
                                                {{--<button type="button" class="btn btn-danger"--}}
                                                        {{--onclick="borrar_codigos_sin_vender({{$dataTypeContent->id}})">--}}
                                                    {{--borrar--}}
                                                {{--</button>--}}
                                            {{--</div>--}}

                                        </div>
                                    @elseif($row->field=='historial_codigos')
                                        <div class="row">

                                            <div class="col-sm-3 col-lg- col-md-3 col-xs-6">
                                                <div class="form-group">
                                                    <label for="name">Filtrar por:</label>
                                                    <select class="form-control select2" name="filtro">
                                                        <option value="0" selected>{{'Elija una aplicacion'}}</option>
                                                        {{--<option value="1">{{ 'Borrados' }}</option>--}}
                                                        <option value="2" >{{ 'Vendidos' }}</option>
                                                    </select>
                                                </div>

                                            </div>
                                            <div  class="col-sm-3 col-lg- col-md-3 col-xs-6" id="{{$row->field.'_select'}}">

                                                <div class="form-group">
                                                    <label for="fecha_inicio">Fecha inicio</label>
                                                    <input id="fecha_inicio"
                                                           type="text"
                                                           class="form-control datepicker"
                                                           data-date-format="'YYYY/MM/DD HH:mm'"
                                                           name="fecha_inicio">
                                                </div>

                                            </div>
                                            <div class="col-sm-3 col-lg- col-md-3 col-xs-6">
                                                <div class="form-group">
                                                    <label for="fecha_final">Fecha final</label>
                                                    <input id="fecha_final"
                                                           type="text"
                                                           class="form-control datepicker"
                                                           data-date-format="'YYYY-MM-DD HH:mm'"
                                                           name="fecha_final">
                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-lg- col-md-3 col-xs-6">
                                                <div class="form-group">
                                                    <label for="name">Accion</label><br>

                                                    <button type="button"
                                                            class="btn btn-info"
                                                            id="mostrar_historial"
                                                            style="margin-top: 0px"
                                                            onclick="mostrar_historial_codigos({{$dataTypeContent->id}})">
                                                        Información
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        @include('voyager::multilingual.input-hidden-bread-read')
                                        <p>{{ $dataTypeContent->{$row->field} }}</p>
                                    @endif
                                @endif
                            </div><!-- panel-body -->
                            @if(!$loop->last)
                                <hr style="margin:0;">
                            @endif
                        </div>
                    @endforeach
                    <div class="table-responsive">
                        <table class="table" id="mytable">
                            <caption id="titulo_tabla"></caption>
                            <thead>
                            <tr>
                                <th scope="col">Codigos</th>
                                <th scope="col">Accion</th>
                                <th scope="col">fecha</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->display_name_singular) }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    @if ($isModelTranslatable)
    <script>

    </script>
    <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script>
        $(document).ready(function () {
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            })
        });


        var params = {};
        function mostrar_historial_codigos(user_id){
            var fecha_inicio = $('#fecha_inicio').val()
            var fecha_final = $('#fecha_final').val()
            if(fecha_inicio.length===0){
                toastr.error("Elija fecha primero")
                return
            }
            if(fecha_final.length===0){
                toastr.error("Elija fecha primero")
                return
            }
            params = {
                user_id:  user_id,
                fecha_inicio:  fecha_inicio,
                fecha_final:  fecha_final,
                _token: '{{ csrf_token() }}'
            }
            $.post('{{ route('historial_codigos') }}', params, function (response) {
                if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {
                    console.log(response.data.data.total_vendido['0'])
                    //$("#codigo_sin_vender").html(response.data.data.cantidad_codigo_sin_vender)
//                    for (var i = 0; i < response.data.data.length; i++) {
//
//                    }
                    $("#mytable").append('<tr><th>' +
                            '' + response.data.data.total_vendido['0'].total+
                            '</th><td>' +
                            'vendidos' +
                            '</td><td>' +
                            '' + response.data.data.codigos_vendidos['0'].fecha +
                            '</td></tr>');

                    toastr.success(response.data.message)
                    console.log(response.data)
                } else {
                    console.log(response.data)
                    toastr.error("Error al recuperar fecha de envio codigo")
                }
            });


        }
        function borrar_codigos_sin_vender(id){
            {{--console.log(id)--}}
            {{--params = {--}}
                {{--user_id:  id,--}}
                {{--_token: '{{ csrf_token() }}'--}}
            {{--}--}}
            {{--$.get('{{ route('borrar_codigo_sin_vender') }}', params, function (response) {--}}
                {{--if ( response--}}
                    {{--&& response.data--}}
                    {{--&& response.data.status--}}
                    {{--&& response.data.status == 200 ) {--}}
                    {{--$("#codigo_sin_vender").html(response.data.data.cantidad_codigo_sin_vender)--}}
                    {{--toastr.success(response.data.message)--}}
                    {{--console.log(response.data)--}}
                {{--} else {--}}
                    {{--console.log(response.data)--}}
                    {{--toastr.error("Error al recuperar fecha de envio codigo")--}}
                {{--}--}}
            {{--});--}}
        }

        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) { // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');
            console.log(form.action);

            $('#delete_modal').modal('show');
        });

    </script>
@stop
