@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').'Promotores')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ 'Promotores' }}
        </h1>
        @can('add',app($dataType->model_name))
            <a href="{{ route('promotor.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new_promotor') }}</span>
            </a>
        @endcan
        @can('delete',app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit',app($dataType->model_name))
            @if(isset($dataType->order_column) && isset($dataType->order_display_column))

            @endif
        @endcan
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if ($isServerSide)
                            <form method="get" class="form-search">
                                <div id="search-input">
                                    <select id="search_key" name="key">
                                        @foreach($searchable as $key)
                                            <option value="{{ $key }}" @if($search->key == $key){{ 'selected' }}@endif>{{ ucwords(str_replace('_', ' ', $key)) }}</option>
                                        @endforeach
                                    </select>
                                    <select id="filter" name="filter">
                                        <option value="contains" @if($search->filter == "contains"){{ 'selected' }}@endif>contains</option>
                                        <option value="equals" @if($search->filter == "equals"){{ 'selected' }}@endif>=</option>
                                    </select>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="{{ __('voyager::generic.search') }}" name="s" value="{{ $search->value }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-lg" type="submit">
                                                <i class="voyager-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        @endif
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    @can('delete',app($dataType->model_name))
                                        <th>
                                            <input type="checkbox" class="select_all">
                                        </th>
                                    @endcan
                                        @php
                                            //dd($dataType->browseRows);


                                                //$numero_identificacion = $dataType->browseRows['11'];

                                                array_forget($dataType->browseRows, '0');
                                                array_forget($dataType->browseRows, '2');
                                                array_forget($dataType->browseRows, '5');
                                                array_forget($dataType->browseRows, '6');
                                                array_forget($dataType->browseRows, '21');
                                                //array_forget($dataType->browseRows, '19');
                                                array_forget($dataType->browseRows, '9');
                                                array_forget($dataType->browseRows, '16');
                                                array_forget($dataType->browseRows, '15');
                                                array_forget($dataType->browseRows, '12');
                                                array_forget($dataType->browseRows, '13');

                                                $row1=$dataType->browseRows['1'];
                                                $dataType->browseRows['1']=$dataType->browseRows['11'];
                                                $dataType->browseRows['11']=$row1;

                                                $row4=$dataType->browseRows['4'];
                                                $dataType->browseRows['4']=$dataType->browseRows['11'];
                                                $dataType->browseRows['11']=$row4;

                                                $row10=$dataType->browseRows['10'];
                                                $row11=$dataType->browseRows['11'];
                                                $row14=$dataType->browseRows['14'];
                                                $dataType->browseRows['10']=$dataType->browseRows['17'];
                                                $dataType->browseRows['11']=$dataType->browseRows['19'];
                                                $dataType->browseRows['14']=$dataType->browseRows['22'];

                                                $dataType->browseRows['17']=$row10;
                                                $dataType->browseRows['19']=$row11;
                                                $dataType->browseRows['22']=$row14;

                                                $dataType->browseRows['17']->field='codigo_enviado';
                                                $dataType->browseRows['17']->display_name='Codigos enviados';
                                                $dataType->browseRows['17']->save();

                                                $dataType->browseRows['22']->field='fecha_envio';
                                                $dataType->browseRows['22']->display_name='Fecha envio';
                                                $dataType->browseRows['22']->save();

                                                $dataType->browseRows['19']->display_name='ID Aplicacion';
                                                $dataType->browseRows['19']->save();

                                                $dataType->browseRows['4']->field='celular';
                                                $dataType->browseRows['4']->save();

                                                //dd($dataType->browseRows);
                                        @endphp

                                    @foreach($dataType->browseRows as $row)
                                        <th>
                                            @if ($isServerSide)
                                                <a href="{{ $row->sortByUrl() }}">
                                                    @endif
                                                    {{ $row->display_name }}
                                                    @if ($isServerSide)
                                                        @if ($row->isCurrentSortField())
                                                            @if (!isset($_GET['sort_order']) || $_GET['sort_order'] == 'asc')
                                                                <i class="voyager-angle-up pull-right"></i>
                                                            @else
                                                                <i class="voyager-angle-down pull-right"></i>
                                                            @endif
                                                        @endif
                                                </a>
                                            @endif
                                        </th>
                                    @endforeach
                                    <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($dataTypeContent as $data)
                                    <tr>
                                        @can('delete',app($dataType->model_name))
                                            <td>
                                                <input type="checkbox" name="row_id" id="checkbox_{{ $data->getKey() }}" value="{{ $data->getKey() }}">
                                            </td>
                                        @endcan


                                        @foreach($dataType->browseRows as $row)

                                            <td>
                                                <?php $options = json_decode($row->details); ?>
                                                @if($row->type == 'image')
                                                    <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:100px">
                                                @elseif($row->type == 'relationship')

                                                    @if($row->display_name=='ID Aplicacion')
                                                        @include('voyager::formfields.id_enviados', ['view' => 'browse', 'emisor_id' => $data->id])
                                                    @else
                                                        @include('voyager::formfields.relationship', ['view' => 'browse'])
                                                    @endif

                                                @elseif($row->type == 'select_multiple')
                                                    @if(property_exists($options, 'relationship'))

                                                        @foreach($data->{$row->field} as $item)

                                                            @if($item->{$row->field . '_page_slug'})
                                                                <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field} }}</a>@if(!$loop->last), @endif
                                                            @else
                                                                {{ $item->{$row->field} }}
                                                            @endif
                                                        @endforeach

                                                        {{-- $data->{$row->field}->implode($options->relationship->label, ', ') --}}
                                                    @elseif(property_exists($options, 'options'))
                                                        @foreach($data->{$row->field} as $item)
                                                            {{ $options->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                        @endforeach
                                                    @endif

                                                @elseif($row->type == 'select_dropdown' && property_exists($options, 'options'))

                                                    @if($data->{$row->field . '_page_slug'})
                                                        <a href="{{ $data->{$row->field . '_page_slug'} }}">{!! $options->options->{$data->{$row->field}} !!}</a>
                                                    @else
                                                        {!! $options->options->{$data->{$row->field}} or '' !!}
                                                    @endif


                                                @elseif($row->type == 'select_dropdown' && $data->{$row->field . '_page_slug'})
                                                    <a href="{{ $data->{$row->field . '_page_slug'} }}">{{ $data->{$row->field} }}</a>
                                                @elseif($row->type == 'date' || $row->type == 'timestamp')
                                                        @if($row->field==='fecha_envio')
{{--                                                            <div class="readmore" id="{{$row->field}}">--}}
{{--                                                                {{--}}
{{--                                                                    mb_strlen( $data->{$row->field} ) > 200--}}
{{--                                                                    ?--}}
{{--                                                                        mb_substr($data->{$row->field}, 0, 200) . ' ...'--}}
{{--                                                                    :--}}
{{--                                                                        $data->{$row->field} }}</div>--}}
                                                            <div class="readmore" id="{{$row->field.''.$data->id}}">
                                                                {{'Elija una ID Aplicacion'}}
                                                            </div>

                                                        @else
                                                            {{ $options && property_exists($options, 'format') ? \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($options->format) : $data->{$row->field} }}
                                                        @endif
                                                @elseif($row->type == 'checkbox')
                                                    @if($options && property_exists($options, 'on') && property_exists($options, 'off'))
                                                        @if($data->{$row->field})
                                                            <span class="label label-info">{{ $options->on }}</span>
                                                        @else
                                                            <span class="label label-primary">{{ $options->off }}</span>
                                                        @endif
                                                    @else
                                                        {{ $data->{$row->field} }}
                                                    @endif
                                                @elseif($row->type == 'color')
                                                    <span class="badge badge-lg" style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                                                @elseif($row->type == 'text')
                                                            @if($row->field==='codigo_enviado')
                                                                @php
                                                                    $codigos_enviados = \Illuminate\Support\Facades\DB::select(
                                                                        "select COUNT(a.estado) as codigo_enviado
                                                                        from activaciones as a
                                                                        where a.emisor_id=$data->id and a.estado='enviada'
                                                                       ");
                                                                    echo ($codigos_enviados['0']->codigo_enviado) ;
                                                                @endphp

                                                            @else
                                                                    <div class="readmore">
                                                                        {{
                                                                            mb_strlen( $data->{$row->field} ) > 200
                                                                            ?
                                                                                mb_substr($data->{$row->field}, 0, 200) . ' ...'
                                                                            :
                                                                                $data->{$row->field} }}</div>
                                                            @endif

                                                    @elseif($row->type == 'number')
                                                        @if($row->field==='codigo_enviado')
                                                            @php
                                                                $codigos_enviados = \Illuminate\Support\Facades\DB::select(
                                                                    "select COUNT(a.estado) as codigo_enviado
                                                                    from activaciones as a
                                                                    where a.emisor_id=$data->id
                                                                   ");
                                                                echo ($codigos_enviados['0']->codigo_enviado) ;
                                                            @endphp

                                                        @else
                                                            <div class="readmore">
                                                                {{
                                                                    mb_strlen( $data->{$row->field} ) > 200
                                                                    ?
                                                                        mb_substr($data->{$row->field}, 0, 200) . ' ...'
                                                                    :
                                                                        $data->{$row->field} }}</div>
                                                        @endif

                                                @elseif($row->type == 'text_area')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    @if(json_decode($data->{$row->field}))
                                                        @foreach(json_decode($data->{$row->field}) as $file)
                                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}" target="_blank">
                                                                {{ $file->original_name ?: '' }}
                                                            </a>
                                                            <br/>
                                                        @endforeach
                                                    @else
                                                        <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($data->{$row->field}) }}" target="_blank">
                                                            Download
                                                        </a>
                                                    @endif
                                                @elseif($row->type == 'rich_text_box')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div class="readmore">{{ mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                                                @elseif($row->type == 'coordinates')
                                                    @include('voyager::partials.coordinates-static-image')
                                                @elseif($row->type == 'multiple_images')
                                                    @php $images = json_decode($data->{$row->field}); @endphp
                                                    @if($images)
                                                        @php $images = array_slice($images, 0, 3); @endphp
                                                        @foreach($images as $image)
                                                            <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:50px">
                                                        @endforeach
                                                    @endif
                                                @else
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <span>{{ $data->{$row->field} }}</span>
                                                @endif
                                            </td>
                                        @endforeach
                                        <td class="no-sort no-click" id="bread-actions">

                                            @foreach(Voyager::actions() as $action)
                                                @if($action=='TCG\Voyager\Actions\EditAction')
                                                    @php $action = new $action($dataType, $data); @endphp
                                                    <a href="{{ route('promotor.edit', $data->id) }}" title="{{ $action->getTitle() }}" {!! $action->convertAttributesToHtml() !!}>
                                                        <i class="{{ $action->getIcon() }}"></i> <span class="hidden-xs hidden-sm">{{ $action->getTitle() }}</span>
                                                    </a>
                                                @elseif($action=='TCG\Voyager\Actions\ViewAction')
                                                    @php $action = new $action($dataType, $data); @endphp
                                                    <a href="{{ route('promotor.show', $data->id) }}" title="{{ $action->getTitle() }}" {!! $action->convertAttributesToHtml() !!}>
                                                        <i class="{{ $action->getIcon() }}"></i> <span class="hidden-xs hidden-sm">{{ $action->getTitle() }}</span>
                                                    </a>
                                                @else
                                                    @include('voyager::promotores.partials.actions', ['action' => $action])
                                                @endif
                                            @endforeach
                                                <a href="{{route('venta-form',$data->id)}}" title="{{ 'Vender' }}"
                                                        class="btn btn-sm btn-success pull-right">
                                                    <i class="voyager-buy"></i> <span class="hidden-xs hidden-sm">{{ 'Vender' }}</span>
                                                </a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if ($isServerSide)
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                                    'voyager::generic.showing_entries', $dataTypeContent->total(), [
                                        'from' => $dataTypeContent->firstItem(),
                                        'to' => $dataTypeContent->lastItem(),
                                        'all' => $dataTypeContent->total()
                                    ]) }}</div>
                            </div>
                            <div class="pull-right">
                                {{ $dataTypeContent->appends([
                                    's' => $search->value,
                                    'filter' => $search->filter,
                                    'key' => $search->key,
                                    'order_by' => $orderBy,
                                    'sort_order' => $sortOrder
                                ])->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
    @endif
@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
        var params = {};

        // $("#user_belongsto_role_relationship").on('change', function() {
        //     var value = $(this).val();
        //     var content = $( "#user_belongsto_role_relationship option:selected" ).text();
        //     get_fecha_envio_codigo(value)
        //     //console.log(value);
        // });
        // $('#user_belongsto_role_relationship').on('change', function(){
        //     //var iii = $('#user_belongsto_role_relationship').val();
        //     alert(iii);
        //     console.log(iii)
        //
        //     //$('#input_select_sucursal').val(iii);
        //     params = {
        //         user_id:     iii,
        //     }
        //     //devolver_cajas();
        //
        // });
        function get_fecha_envio_codigo(activacion_id, data_id){
            params = {
                activacion_id:  activacion_id,
                _token: '{{ csrf_token() }}'
            }
            $.post('{{ route('get_fecha_envio_codigo') }}', params, function (response) {
                if ( response
                    && response.data
                    && response.data.status
                    && response.data.status == 200 ) {
                    $("#fecha_envio" + data_id).html(response.data.data.fecha)
                    toastr.success(response.data.message)
                    console.log(response.data)
                } else {
                    console.log(response.data)
                    toastr.error("Error al recuperar fecha de envio codigo")
                }
            });
        }
        // function cambia_contenido(){
        //     $("#capa").html('Nuevo contenido');
        // }



        $(document).ready(function () {
                    @if (!$dataType->server_side)
            var table = $('#dataTable').DataTable({!! json_encode(
                    array_merge([
                        "order" => [],
                        "language" => __('voyager::datatable'),
                        "columnDefs" => [['targets' => -1, 'searchable' =>  false, 'orderable' => false]],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!});
            @else
                $('#search-input select').select2({
                minimumResultsForSearch: Infinity
            });
            @endif

            @if ($isModelTranslatable)
                $('.side-body').multilingual();
            //Reinitialise the multilingual features when they change tab
            $('#dataTable').on('draw.dt', function(){
                $('.side-body').data('multilingual').init();
            })
            @endif
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });
    </script>
@stop
