@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ 'Datos de afiliación para promotor' }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="
                                @if(!is_null($dataTypeContent->getKey()))
                                    {{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}
                                @else
                                    {{ route('promotor.store') }}
                                @endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{'addRows'};
                            @endphp
@php
    //dd($dataTypeRows);
        //rol modificar a ciudad
        $rowciudad=$dataTypeRows['8'];
        $rowmunicipio=$dataTypeRows['9'];
        //$dataTypeRows['10']=$rowci;


        //rol modificar a idaplicacion
        $rowci=$dataTypeRows['5'];
        $dataTypeRows['8']=$rowci;
        $dataTypeRows['8']['display_name']='ID Aplicacion';
        //dd($dataTypeRows['8']['display_name']);

        //dd($dataTypeRows);
        $rowci=$dataTypeRows['13'];
        $dataTypeRows['1']=$rowci;
        array_pull($dataTypeRows, '2');
        array_pull($dataTypeRows, '3');
        $rowci=$dataTypeRows['6'];
        $dataTypeRows['5']=$rowci;
        //direccion
        $rowci=$dataTypeRows['14'];
        $dataTypeRows['6']=$rowci;
        //numero de identificacion
        $rowci=$dataTypeRows['15'];
        $dataTypeRows['7']=$rowci;

        //numero pais
        $rowci=$dataTypeRows['10'];
        $dataTypeRows['9']=$rowci;
        $dataTypeRows['10']=$rowciudad;
        $dataTypeRows['11']=$rowmunicipio;

        array_pull($dataTypeRows, '12');
        array_pull($dataTypeRows, '13');
        array_pull($dataTypeRows, '14');
        array_pull($dataTypeRows, '15');
        $row0 = $dataTypeRows['0'];
        $row1 = $dataTypeRows['1'];
        $row5 = $dataTypeRows['5'];
        $row6 = $dataTypeRows['6'];
        $row7 = $dataTypeRows['7'];
        $row8 = $dataTypeRows['8'];

        $dataTypeRows['0']=$row8;
        $dataTypeRows['1']=$row0;
        $dataTypeRows['5']=$row1;
        $dataTypeRows['6']=$row5;
        $dataTypeRows['7']=$row6;
        $dataTypeRows['8']=$row7;
        //dd($dataTypeRows);
@endphp
                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->

                                @php
                                    $options = json_decode($row->details);
                                    $display_options = isset($options->display) ? $options->display : NULL;
                                @endphp
                                @if ($options && isset($options->legend) && isset($options->legend->text))
                                    <legend class="text-{{$options->legend->align or 'center'}}"
                                            style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">
                                            {{$options->legend->text}}
                                    </legend>
                                @endif
                                @if ($options && isset($options->formfields_custom))
                                    @include('voyager::formfields.custom.' . $options->formfields_custom)
                                @else
                                    <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                        {{ $row->slugify }}
                                        <label for="name">{{ $row->display_name }}</label>
                                        @include('voyager::multilingual.input-hidden-bread-edit-add')
                                        @if($row->type == 'relationship')
                                            @include('voyager::formfields.idAplicacion')
                                        @else
                                            {{--input text--}}

                                            <input
                                                @if($row->required == 1)
                                                    required
                                                @endif
                                                    type="text"
                                                    class="form-control"
                                                    name="{{ $row->field }}"
                                                    id="{{ $row->field }}"
                                                    placeholder="{{ isset($options->placeholder)? old($row->field, $options->placeholder): $row->display_name }}"
                                                   {!! isBreadSlugAutoGenerator($options) !!}
                                                   value="@if(isset($dataTypeContent->{$row->field})){{ old($row->field, $dataTypeContent->{$row->field}) }}@elseif(isset($options->default)){{ old($row->field, $options->default) }}@else{{ old($row->field) }}@endif">

                                        @endif

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach
                                    </div>
                                @endif
                            @endforeach

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        function devolver_cajas() {
            $.get("{{route('get_user')}}",params,  function (response1) {
                if ( response1) {

                    $("#name").val(response1.data.data.name);
                    $("#ci").val(response1.data.data.ci);
                    $("#celular").val(response1.data.data.celular);
                    //Materialize.toast('exito', 3200);
//                    $("#select").html(response.data);
                    console.log(response1.data.data);

                    //$('#name').value=response1.data.data.name;
                    //document.getElementById("name").innerText=response1.data.data.name;



                } else {
                    console.log(response1);
                    //Materialize.toast('Error', 3200);
                }
            });

        }

        $('#id_aplicacion').on('change', function(){
            var iii = $('#id_aplicacion').val();
            //alert(iii);

            //$('#input_select_sucursal').val(iii);
            params = {
                user_id:     iii,
            }
            devolver_cajas();
        });



        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
