@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{'Editar promotor' }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ route('promotor.update', $dataTypeContent->getKey()) }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                                //dd($dataTypeRows);
                                array_forget($dataTypeRows, '1');
                                //array_forget($dataTypeRows, '2');
                                array_forget($dataTypeRows, '3');
                                array_forget($dataTypeRows, '4');
                                //array_forget($dataTypeRows, '5');
                                array_forget($dataTypeRows, '6');
                                array_forget($dataTypeRows, '7');
                                array_forget($dataTypeRows, '8');
                                array_forget($dataTypeRows, '11');
                                array_forget($dataTypeRows, '12');
                                array_forget($dataTypeRows, '13');
                                $row_fecha_envio = array_pull($dataTypeRows, '17');
                                $row_estado = array_pull($dataTypeRows, '18');
                                $row5=$dataTypeRows['5'];
                                $dataTypeRows['5']=$dataTypeRows['14'];
                                $dataTypeRows['14']=$row5;

                                $row16=$dataTypeRows['16'];
                                $dataTypeRows['16']=$dataTypeRows['19'];
                                $dataTypeRows['19']=$row16;
                                array_add($dataTypeRows, '17', $row_fecha_envio);
                                array_add($dataTypeRows, '18', $row_estado);
   $dataTypeRows['17']['display_name']='Fecha Inscripcion';
    $dataTypeRows['17']['field']='fecha_inscripcion';
    $dataTypeRows['17']->save;

array_forget($dataTypeRows, '0');
                                //dd($dataTypeRows);

                            @endphp



                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $options = json_decode($row->details);
                                    $display_options = isset($options->display) ? $options->display : NULL;
                                @endphp
                                @if ($options && isset($options->legend) && isset($options->legend->text))
                                    <legend class="text-{{$options->legend->align or 'center'}}" style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">{{$options->legend->text}}</legend>
                                @endif
                                @if ($options && isset($options->formfields_custom))
                                    @include('voyager::formfields.custom.' . $options->formfields_custom)
                                @else
                                    <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                        {{ $row->slugify }}
                                        <label for="name">{{ $row->display_name }}</label>
                                        @include('voyager::multilingual.input-hidden-bread-edit-add')
                                        @if($row->type == 'relationship')
                                            @if($row->field == 'Pais')
                                                readonly
                                            @else
                                                @include('voyager::formfields.relationship')
                                            @endif



                                        @else




                                            @if($row->field === 'estado_promotor')
                                                <br>
                                                <?php $checked = false; ?>
                                                @if(isset($dataTypeContent->{$row->field}) || old($row->field))
                                                    <?php $checked = old($row->field, $dataTypeContent->{$row->field}); ?>
                                                @else
                                                    <?php $checked = isset($options->checked) && $options->checked ? true : false; ?>
                                                @endif
                                                @if(isset($options->on) && isset($options->off))

                                                    <input type="checkbox" name="{{ $row->field }}" class="toggleswitch"
                                                           data-on="{{ $options->on }}" {!! $checked ? 'checked="checked"' : '' !!}
                                                           data-off="{{ $options->off }}">
                                                @else

                                                    <input type="checkbox" name="{{ $row->field }}" class="toggleswitch"
                                                           id="{{ $row->field }}"
                                                           @if($checked) checked @endif>
                                                @endif
                                            @elseif($row->field === 'fecha_inscripcion')
                                                <input @if($row->required == 1)
                                                       required
                                                       @endif
                                                       @if($row->field == 'name' || $row->field == 'ci')
                                                       readonly
                                                       @endif
                                                       type="text"
                                                       class="form-control"
                                                       name="{{ $row->field }}"
                                                       placeholder="{{ isset($options->placeholder)? old($row->field, $options->placeholder): $row->display_name }}"
                                                       {!! isBreadSlugAutoGenerator($options) !!}
                                                       value="@if(isset($dataTypeContent->{$row->field})){{ old($row->field, $dataTypeContent->{$row->field}) }}@elseif(isset($options->default)){{ old($row->field, $options->default) }}@else{{ old($row->field) }}@endif">

                                            @else
                                                <input @if($row->required == 1)
                                                       required
                                                       @endif
                                                       @if($row->field == 'name' || $row->field == 'ci')
                                                       readonly
                                                       @endif
                                                       type="text"
                                                       class="form-control"
                                                       name="{{ $row->field }}"
                                                       placeholder="{{ isset($options->placeholder)? old($row->field, $options->placeholder): $row->display_name }}"
                                                       {!! isBreadSlugAutoGenerator($options) !!}
                                                       value="@if(isset($dataTypeContent->{$row->field})){{ old($row->field, $dataTypeContent->{$row->field}) }}@elseif(isset($options->default)){{ old($row->field, $options->default) }}@else{{ old($row->field) }}@endif">

                                            @endif





                                        @endif

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach
                                    </div>
                                @endif
                            @endforeach

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
