@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        Crear promotor
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add"
                          action="{{ route('promotor.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if(!is_null($dataTypeContent->getKey()))
                        {{ method_field("PUT") }}
                    @endif

                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp

                                @php
                                    //rol modificar a ciudad

    $rowciudad=$dataTypeRows['8'];
    $rowmunicipio=$dataTypeRows['9'];
    //$dataTypeRows['10']=$rowci;


    //rol modificar a idaplicacion
//dd($dataTypeRows);
    $dataTypeRows['8']['display_name']='ID Aplicacion';
    $dataTypeRows['8']->save;
    //dd($dataTypeRows['8']['display_name']);


    $rowci=$dataTypeRows['13'];
    $dataTypeRows['1']=$rowci;
    //array_pull($dataTypeRows, '2');
    //array_pull($dataTypeRows, '3');
    $rowci=$dataTypeRows['6'];
    $dataTypeRows['5']=$rowci;
    //direccion
    $rowci=$dataTypeRows['14'];
    $dataTypeRows['6']=$rowci;
    //numero de identificacion
    $rowci=$dataTypeRows['15'];
    $dataTypeRows['7']=$rowci;

    //numero pais
    //$rowci=$dataTypeRows['10'];
    $dataTypeRows['9']=$rowci;
    //$dataTypeRows['10']=$rowciudad;
    //$dataTypeRows['11']=$rowmunicipio;


    //array_pull($dataTypeRows, '12');
    //array_pull($dataTypeRows, '13');
   // array_pull($dataTypeRows, '14');
   // array_pull($dataTypeRows, '15');
    $row0 = $dataTypeRows['0'];
    $row1 = $dataTypeRows['1'];
    $row5 = $dataTypeRows['5'];
    $row6 = $dataTypeRows['6'];
    $row7 = $dataTypeRows['7'];
    $row8 = $dataTypeRows['8'];

    $dataTypeRows['0']=$row6;
    $dataTypeRows['1']=$row0;

    $dataTypeRows['1']->field='name';
    $dataTypeRows['1']->display_name='Nombre';
    $dataTypeRows['1']->save;
    $dataTypeRows['5']=$row1;
    $dataTypeRows['6']=$row5;
    $dataTypeRows['7']=$row6;
    $dataTypeRows['8']=$row7;

    $row7=$dataTypeRows['7'];
//    $dataTypeRows['7']=$dataTypeRows['10'];
    //$dataTypeRows['10']=$row7;



    array_pull($dataTypeRows, '3');
    array_pull($dataTypeRows, '4');

    //dd($dataTypeRows);

    $rowci=$dataTypeRows['0'];
    $dataTypeRows['0']=$dataTypeRows['6'];
    $dataTypeRows['6']=$rowci;
    $dataTypeRows['0']->display_name='ID Aplicacion';

      $rowci=$dataTypeRows['6'];
    $dataTypeRows['6']=$dataTypeRows['2'];
    $dataTypeRows['2']=$rowci;

    $dataTypeRows['2']['display_name']='Carnet de Identidad';
    $dataTypeRows['2']['field']='ci';
    $dataTypeRows['2']->save;



        $dataTypeRows['6']['display_name']='Numero de Telefono';
        $dataTypeRows['6']['field']='celular';
    $dataTypeRows['6']->save;
     $array7 = array_pull($dataTypeRows, '7');

     //$numero_identificacion = array_pull($dataTypeRows, '15');



      $rowdireccion=$dataTypeRows['16'];
    $dataTypeRows['16']=$dataTypeRows['8'];
    $dataTypeRows['8']=$rowdireccion;
array_forget($dataTypeRows, '12');
array_forget($dataTypeRows, '13');
array_forget($dataTypeRows, '14');
array_forget($dataTypeRows, '16');
array_forget($dataTypeRows, '17');
array_forget($dataTypeRows, '9');
array_forget($dataTypeRows, '5');

$link = array_pull($dataTypeRows, '20');
    array_add($dataTypeRows, '7', $dataTypeRows['15']);
    $dataTypeRows['15']=$link;

    $fecha_inscripcion = $dataTypeRows['18'];
    $dataTypeRows['18']=$dataTypeRows['7'];

    $dataTypeRows['7']=$fecha_inscripcion;
    $dataTypeRows['7']['display_name']='Fecha Inscripcion';
    $dataTypeRows['7']['field']='fecha_inscripcion';
    $dataTypeRows['7']->save;

    $estado_promotor = array_pull($dataTypeRows, '19');
    array_add($dataTypeRows, '19', $estado_promotor);

    $dataTypeRows['18']['display_name']='Numero identificacion';
    $dataTypeRows['18']['field']='numero_identificacion';
    $dataTypeRows['18']->save;

                                        //dd($dataTypeRows);
                                  @endphp


                            @foreach($dataTypeRows as $row)
                            <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $options = json_decode($row->details);
                                    $display_options = isset($options->display) ? $options->display : NULL;
                                @endphp
                                @if ($options && isset($options->legend) && isset($options->legend->text))
                                    <legend class="text-{{$options->legend->align or 'center'}}" style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">{{$options->legend->text}}</legend>
                                @endif
                                @if ($options && isset($options->formfields_custom))
                                    @include('voyager::formfields.custom.' . $options->formfields_custom)
                                @else
                                    <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                        {{ $row->slugify }}

                                        <label for="name">{{ $row->display_name }}</label>
                                        @include('voyager::multilingual.input-hidden-bread-edit-add')
                                        @if($row->type == 'relationship')
                                            @if($row->display_name === 'ID Aplicacion')
                                                @include('voyager::formfields.idAplicacion')
                                            @else
                                                @include('voyager::formfields.add_relationship')
                                            @endif

                                        @else
                                            @if($row->field === 'fecha_inscripcion')
                                                <input id="fecha_inscripcion" @if($row->required == 1) required @endif type="datetime" class="form-control datepicker" name="{{ $row->field }}"
                                                       value="@if(isset($dataTypeContent->{$row->field})){{ \Carbon\Carbon::parse(old($row->field, $dataTypeContent->{$row->field}))->format('m/d/Y g:i A') }}@else{{old($row->field)}}@endif">

                                            @elseif($row->field === 'estado_promotor')
                                                <br>
                                                <?php $checked = false; ?>
                                                @if(isset($dataTypeContent->{$row->field}) || old($row->field))
                                                    <?php $checked = old($row->field, $dataTypeContent->{$row->field}); ?>
                                                @else
                                                    <?php $checked = isset($options->checked) && $options->checked ? true : false; ?>
                                                @endif
                                                @if(isset($options->on) && isset($options->off))

                                                    <input type="checkbox" name="{{ $row->field }}" class="toggleswitch"
                                                           data-on="{{ $options->on }}" {!! $checked ? 'checked="checked"' : '' !!}
                                                           data-off="{{ $options->off }}">
                                                @else

                                                    <input type="checkbox" name="{{ $row->field }}" class="toggleswitch"
                                                           id="{{ $row->field }}"
                                                           @if($checked) checked @endif>
                                                @endif
                                            @else

                                                <input

                                                        required

                                                        type="text"
                                                        class="form-control"
                                                        name="{{ $row->field }}"
                                                        id="{{ $row->field }}"
                                                        placeholder="{{ isset($options->placeholder)? old($row->field, $options->placeholder): $row->display_name }}"
                                                        {!! isBreadSlugAutoGenerator($options) !!}
                                                        value="@if(isset($dataTypeContent->{$row->field})){{ old($row->field, $dataTypeContent->{$row->field}) }}@elseif(isset($options->default)){{ old($row->field, $options->default) }}@else{{ old($row->field) }}@endif"
                                                >
                                            @endif



                                        @endif

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach
                                    </div>
                                @endif
                            @endforeach

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                               onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        //change ID Aplication
        $('#user_belongsto_role_relationship').on('change', function(){
            var iii = $('#id_aplicacion').val();
            //alert(iii);

            //$('#input_select_sucursal').val(iii);
            params = {
                user_id:     iii,
            }
            devolver_cajas();

        });
        var params = {}
        var $image

        function buscarSelectPais(id){
            var select=document.getElementById("user_belongsto_paise_relationship");
            var buscar=id;
            for(var i=1;i<select.length;i++){
                if(select.options[i].value==buscar){
                    select.selectedIndex=i
                    //cambiar texto de span select dinamicamente
                    document.getElementById("select2-user_belongsto_paise_relationship-container").innerHTML=select.options[i].text
                    //alert(span)
                }

            }
        }
        function buscarSelectDepartamento(id){
            var select=document.getElementById("user_belongsto_departamento_relationship");
            var buscar=id;
            for(var i=1;i<select.length;i++){
                if(select.options[i].value==buscar){
                    select.selectedIndex=i
                    //cambiar texto de span select dinamicamente
                    document.getElementById("select2-user_belongsto_departamento_relationship-container").innerHTML=select.options[i].text
                    //alert(span)
                }

            }
        }
        function buscarSelectProvincia(id){
            var select=document.getElementById("user_belongsto_provincia_relationship");
            var buscar=id;
            for(var i=1;i<select.length;i++){
                if(select.options[i].value==buscar){
                    select.selectedIndex=i
                    //cambiar texto de span select dinamicamente
                    document.getElementById("select2-user_belongsto_provincia_relationship-container").innerHTML=select.options[i].text
                    //alert(span)
                }

            }
        }
        function buscarSelectMunicipio(id){
            var select=document.getElementById("user_belongsto_municipio_relationship");
            var buscar=id;
            for(var i=1;i<select.length;i++){
                if(select.options[i].value==buscar){
                    select.selectedIndex=i
                    //cambiar texto de span select dinamicamente
                    document.getElementById("select2-user_belongsto_municipio_relationship-container").innerHTML=select.options[i].text
                    //alert(span)
                }

            }
        }
        function buscarEstadoPromotor(id){
            //alert(id)
            var div = document.getElementById('estado_promotor');
            div.style.display='none'
            // var buscar=id;
            // for(var i=1;i<select.length;i++){
            //     if(select.options[i].value==buscar){
            //         select.selectedIndex=i
            //         //cambiar texto de span select dinamicamente
            //         document.getElementById("select2-user_belongsto_municipio_relationship-container").innerHTML=select.options[i].text
            //         //alert(span)
            //     }
            // }
        }


        function devolver_cajas(){
            $.get("{{route('get_user')}}",params,  function (response1) {
                if ( response1) {

                    $("#name").val(response1.data.data.name);
                    $("#ci").val(response1.data.data.ci);
                    $("#celular").val(response1.data.data.celular);
                    $("#direccion").val(response1.data.data.direccion);
                    $("#numero_identificacion").val(response1.data.data.numero_identificacion);
                    buscarSelectPais(response1.data.data.Pais);
                    buscarSelectDepartamento(response1.data.data.departamento_id);
                    buscarSelectProvincia(response1.data.data.provincia_id);
                    buscarSelectMunicipio(response1.data.data.Municipio);
                    buscarEstadoPromotor(response1.data.data.estado_promotor);
                    $("#Ciudad").val(response1.data.data.Ciudad);
                    $("#Municipio").val(response1.data.data.Municipio);
                    $("#fecha_inscripcion").val(response1.data.data.fecha_inscripcion);
                    $("#link").val(response1.data.data.link);

                    //Materialize.toast('exito', 3200);
//                    $("#select").html(response.data);
                    console.log(response1.data.data);

                    //$('#name').value=response1.data.data.name;
                    //document.getElementById("name").innerText=response1.data.data.name;



                } else {
                    console.log(response1);
                    //Materialize.toast('Error', 3200);
                }
            });

        }
        function devolverDepartamentos(){
            $.get("{{route('get_departamentos')}}",params,  function (response1) {
                if ( response1) {
                    if(response1.data.data.length==0){
                        $("#user_belongsto_departamento_relationship").
                        append('<option value="0">Registre departamentos</option>');
                    }else{
                        $("#user_belongsto_departamento_relationship").
                        append('<option value="0">Elija departamento</option>');
                    }
                    for (var i = 0; i < response1.data.data.length; i++) {
                        $("#user_belongsto_departamento_relationship").
                        append('<option value="'+ response1.data.data[i].id +'">'+ response1.data.data[i].nombre +'</option>');
                    }
                    //console.log(response1.data.data);
                } else {
                    //console.log(response1);
                    //Materialize.toast('Error', 3200);
                }
            });
        }
        function devolverProvincias(){
            $.get("{{route('get_provincias')}}",params,  function (response1) {
                if ( response1) {
                    if(response1.data.data.length==0){
                        $("#user_belongsto_provincia_relationship").
                        append('<option value="0">Registre provincias</option>');
                    }else{
                        $("#user_belongsto_provincia_relationship").
                        append('<option value="0">Elija provincia</option>');
                    }
                    for (var i = 0; i < response1.data.data.length; i++) {
                        $("#user_belongsto_provincia_relationship").
                        append('<option value="'+ response1.data.data[i].id +'">'+ response1.data.data[i].nombre +'</option>');
                    }
//                    console.log(response1.data.data);
                } else {
                    //console.log(response1);
                    //Materialize.toast('Error', 3200);
                }
            });
        }
        function devolverMunicipios(){
            $.get("{{route('get_municipios')}}",params,  function (response1) {
                if ( response1) {
                    if(response1.data.data.length==0){
                        $("#user_belongsto_municipio_relationship").
                        append('<option value="0">Registre municipios</option>');
                    }else{
                        $("#user_belongsto_municipio_relationship").
                        append('<option value="0">Elija municipio</option>');
                    }
                    for (var i = 0; i < response1.data.data.length; i++) {
                        $("#user_belongsto_municipio_relationship").
                        append('<option value="'+ response1.data.data[i].id +'">'+ response1.data.data[i].nombre +'</option>');
                    }
//                    console.log(response1.data.data);
                } else {
                    //console.log(response1);
                    //Materialize.toast('Error', 3200);
                }
            });
        }





        //change ID Aplication
        $('#id_aplicacion').on('change', function(){
            var iii = $('#id_aplicacion').val();
            //alert(iii);

            //$('#input_select_sucursal').val(iii);
            params = {
                user_id:     iii,
            }
            devolver_cajas();

        });

        //change select pais
        $('#user_belongsto_paise_relationship').on('change', function(){
            $("#user_belongsto_departamento_relationship").empty();
            $("#user_belongsto_provincia_relationship").empty();
            $("#user_belongsto_municipio_relationship").empty();
            var iii = $("#user_belongsto_paise_relationship").val();
            //alert(iii);

            //$('#input_select_sucursal').val(iii);
            params = {
                pais_id:     iii,
            }
            devolverDepartamentos();
        });

        //change select departamentos
        $('#user_belongsto_departamento_relationship').on('change', function(){
            //$("#user_belongsto_departamento_relationship").empty();
            $("#user_belongsto_provincia_relationship").empty();
            $("#user_belongsto_municipio_relationship").empty();
            var iii = $("#user_belongsto_departamento_relationship").val();
            //alert(iii);

            //$('#input_select_sucursal').val(iii);
            params = {
                departamento_id:     iii,
            }
            devolverProvincias();
        });

        //change select provincias
        $('#user_belongsto_provincia_relationship').on('change', function(){
            $("#user_belongsto_municipio_relationship").empty();
            var iii = $("#user_belongsto_provincia_relationship").val();
            //alert(iii);

            //$('#input_select_sucursal').val(iii);
            params = {
                provincia_id:     iii,
            }
            devolverMunicipios();
        });


        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
