@extends('voyager::master')
@section('content')
    <div class="container">

        <div class="row bg-info">
            <h3>Formulario de venta de códigos de activación</h3>
            <p><b>Cliente: </b> {{$cliente->name}}</p>
            <p><b>Carnet de Identidad: </b> {{$cliente->ci}}</p>
            <p><b>ID aplicacion: </b> {{$cliente->celular}}</p>
            <p><b>Fecha: </b> {{$fecha}}</p>
        </div>

        <div class="row bg-warning">
            <h3>Productos</h3>
            @foreach($productos as $producto)
                <div class="col-xs-6 col-md-3">
                    <a href="#" class="thumbnail">
                        <img src="{{asset('storage/'.$producto->imagen)}}"
                             width="200" height="200">
                    </a>
                    <h3>{{$producto->nombre}}</h3>
                    <p><b>precio: </b>{{$producto->precio}}</p>
                    <p><b>duracion: </b>{{$producto->dias_duracion}}</p>
                    <p><b>Cantidad:</b>  <input type="number" class="form-control"  name="cantidad{{$producto->id}}" id="cantidad{{$producto->id}}" min="1" value="1">
                    </p>
                    <p>
                        <button class="btn btn-warning btn-block"
                                onclick="agregarProducto({{json_encode($producto)}},'cantidad{{$producto->id}}',this )">
                            agregar
                        </button>




                    </p>
                </div>
            @endforeach
        </div>

        <div>
            <h3>Detalle de venta</h3>
            <!--         <form action=""> -->
        <!--             {{ csrf_field() }} -->
            <table class="table" id="table">
                <thead>
                <tr>
                    <th>id</th>
                    <th>producto</th>
                    <th>precio</th>
                    <th>cantidad</th>
                    <th>subtotal</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <h1 id="total"></h1>
            <button  class="btn btn-success pull-right" id="send" onclick="vender()">realizar venta</button>
            <!--         </form> -->
        </div>
    </div>





@endsection




D
<script type="text/javascript">
    var total_value = 0;
    var tab;

    function agregarProducto(producto, cantidad_id, btn){
        btn.disabled='disabled';

        var cantidad_value = document.getElementById(cantidad_id).value;

        var table = document.getElementById("table");
        var row = table.insertRow(1);

        var producto_id = row.insertCell(0)
        var nombre = row.insertCell(1);
        var precio = row.insertCell(2);
        var cantidad = row.insertCell(3);
        var subtotal = row.insertCell(4);

        producto_id.innerHTML = producto.id;
        nombre.innerHTML = producto.nombre;
        precio.innerHTML = producto.precio;
        cantidad.innerHTML = cantidad_value;
        subtotal.innerHTML = producto.precio*cantidad_value;

        total_value = total_value+(producto.precio*cantidad_value);

        document.getElementById("total").innerHTML = 'TOTAL Bs. '+total_value;
        //alert(tab);
        tab = $('#table').tableToJSON();
        console.log(tab)

    }


    function vender(){
        $.ajax({
            type: "POST",
            url:"{{route('venta_codigos', $cliente->id)}}",
            data: {
                table: tab,
                total: total_value
            },
            success: function( msg ) {
                location.href = "{{route('voyager.ventas.index')}}";
                $("#ajaxResponse").append("<div>"+msg+"</div>");

            }
        });
    }








</script>




