@extends('voyager::master')

@section('content')
    <div class="container">

        <div class="panel panel-default">

            <ol class="breadcrumb">
                <li><a href="#">Inicio</a></li>
                {{--<li><a href="#">{{$psa->nombre_ciudad}}</a></li>--}}
                <li><a href="#">{{$gestion->nombre}}</a></li>
            </ol>

            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center"> MATERIAS DE LA GESTION {{$gestion->nombre}} </h2>
                </div>
            </div>

            <div class="row">
                {{--<div class="col-md-4">--}}
                    {{--<h3>Materias:</h3>--}}
                {{--</div>--}}
                <div class="col-md-2">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#myModal">
                        nueva materia
                    </button>
                </div>
            </div>


            <div class="panel-body">

                <div class="row">

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <th>id</th>
                            <th>nombre</th>
                            <th>link de texto</th>
                            <th>acciones</th>
                            </thead>
                            <tbody>
                            @foreach($materias as $materia)
                                <tr>
                                    <td>{{$materia->id}}</td>
                                    <td>{{$materia->nombre}}</td>
                                    <td>{{$materia->link_texto}}</td>
                                    <td>
                                        <a class="btn btn-primary" target ="_blank" href="{{route('ver-preguntas-psa',$materia->id)}}">ver preguntas</a>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminar{{$materia->id}}">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="eliminar{{$materia->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Eliminar gestion </h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Esta seguro que quiere eliminar esta gestion?</p>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                            <a class="btn btn-danger" href="{{route('eliminar-materia-psa',$materia->id)}}">Si</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="exampleModalLabel">Nueva materia</h4>
                </div>
                <div class="modal-body">

                    <form method="POST" action="{{route('guardar-materia-psa',$gestion->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" id="nombre"  name="nombre" >
                        </div>

                        <div class="form-group">
                            <label for="name">Link de texto</label>
                            <input type="text" class="form-control" id="link_texto"  name="link_texto" >
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="but">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection