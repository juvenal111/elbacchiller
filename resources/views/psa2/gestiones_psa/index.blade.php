@extends('voyager::master')

@section('content')
    <div class="container">

        <div class="panel panel-default">

            <ol class="breadcrumb">
                <li><a href="#">Inicio</a></li>
                <li><a href="#">{{$psa->nombre_ciudad}}</a></li>
            </ol>

            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">{{$psa->nombre_ciudad}} </h2>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4">
                    <h3>Gestiones:</h3>
                </div>
                <div class="col-md-2">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#myModal">
                        nueva gestion
                    </button>
                </div>
            </div>

            <div class="panel-body">

                <div class='row'>

                    <div class="table-responsive">
                        <table class="table table-hover table-condensed">

                            <thead>
                            <th>id</th>
                            <th>nombre</th>
                            <th>acciones</th>
                            </thead>

                            <tbody>
                            @foreach($gestiones as $gestion)
                                <tr>
                                    <td>{{$gestion->id}}</td>
                                    <td>{{$gestion->nombre}}</td>

                                    <td>
                                        <a class="btn btn-primary" target ="_blank" href="{{route('ver-materias-psa',$gestion->id)}}">ver materias</a>
                                        <a class="btn btn-primary" target ="_blank" href="{{route('ver-areas-psa',$gestion->id)}}">ver areas</a>

                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminar{{$gestion->id}}">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="eliminar{{$gestion->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Eliminar gestion </h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Esta seguro que quiere eliminar esta gestion?</p>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                            <a class="btn btn-danger" href="{{route('eliminar-gestion-psa',$gestion->id)}}">Si</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>

                </div>


            </div>

    </div>
    </div>




            {{--<div class="row">--}}
                {{--<div class="panel-body">--}}
                    {{--<div class="table-responsive">--}}
                        {{--<table class="table table-striped">--}}
                            {{--<thead>--}}
                            {{--<th>id</th>--}}
                            {{--<th>nombre</th>--}}
                            {{--<th>acciones</th>--}}
                            {{--</thead>--}}
                            {{--<tbody>--}}
                            {{--@foreach($gestiones as $gestion)--}}
                                {{--<tr>--}}
                                    {{--<td>{{$gestion->id}}</td>--}}
                                    {{--<td>{{$gestion->nombre}}</td>--}}
                                    {{--<td>--}}
                                        {{--<a class="btn btn-primary" href="{{route('ver-materias-psa',$gestion->id)}}">ver materias</a>--}}
                                        {{--<button class="btn btn-warning" href="#">editar</button>--}}
                                        {{--<button class="btn btn-danger" href="#">borrar</button>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}
                            {{--</tbody>--}}
                        {{--</table>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}





    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="exampleModalLabel">Nueva gestion</h4>
                </div>
                <div class="modal-body">

                    <form method="POST" action="{{route('guardar-gestion-psa',$psa->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" id="nombre"  name="nombre" >
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="but">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection