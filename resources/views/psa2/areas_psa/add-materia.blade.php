@extends('voyager::master')

@section('content')

    <div class="container">

        <div class="panel panel-default">

            <ol class="breadcrumb">
                <li><a href="#">Inicio</a></li>
                <li><a href="#">PSA Ciudad</a></li>
                {{--<li><a href="#">{{$materia->nombre_gestion}}</a></li>--}}
                <li class="active">{{$area->nombre}}</li>
            </ol>

            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center"> {{$area->nombre}} </h2>
                </div>
            </div>




            <div class="panel-body">


                <div class="row  bg-warning">
                    <div class="col-md-8">
                        <form class="form-horizontal" action="{{route('editar-area-psa',$area->id)}}" method="POST">
                            {{ csrf_field() }}
                            <br>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="link_texto">Nombre:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nombre" name="nombre" value="{{$area->nombre}}">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-warning pull-right">editar</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>




                <div class="row">
                    <div class="col-md-4">
                        <h3>Materias:</h3>
                    </div>
                    <div class="col-md-2">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#myModal">
                            agregar materia
                        </button>
                    </div>
                </div>



                <div class='row'>

                    <div class="table-responsive">
                        <table class="table table-hover table-condensed">

                            <thead>
                            <th>id</th>
                            <th>nombre</th>
                            <th>acciones</th>
                            </thead>

                            <tbody>
                            @foreach($materias as $materia)
                                <tr>
                                    <td>{{$materia->id}}</td>
                                    <td>{{$materia->nombre}}</td>

                                    <td>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminar{{$materia->id}}">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="eliminar{{$materia->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">quitar materia</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Esta seguro que quiere quitar esta pregunta con todas sus respuesta?</p>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                            <a class="btn btn-danger" href="{{route('quitar-materia-psa',[$materia->id,$area->id])}}">Si</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>

                </div>




            </div>

        </div>

    </div>















    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="exampleModalLabel">Agregar maeteria</h4>
                </div>
                <div class="modal-body">

                    <form method="POST" action="{{route('agregar-materia-psa',$area->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}


                        <select id="materia" name="materia" class="form-control">
                            @foreach($todas_materias as $materia)
                                <option value="{{$materia->id}}">{{$materia->nombre}}</option>
                            @endforeach

                        </select>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="but">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>










@endsection