<?php $__env->startSection('content'); ?>
    <div class="container">

        <div class="row bg-info">
            <h3>Formulario de venta de códigos de activación</h3>
            <p><b>Cliente: </b> <?php echo e($cliente->name); ?></p>
            <p><b>Carnet de Identidad: </b> <?php echo e($cliente->ci); ?></p>
            <p><b>ID aplicacion: </b> <?php echo e($cliente->celular); ?></p>
            <p><b>Fecha: </b> <?php echo e($fecha); ?></p>
        </div>

        <div class="row bg-warning">
            <h3>Productos</h3>
            <?php $__currentLoopData = $productos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $producto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-xs-6 col-md-3">
                    <a href="#" class="thumbnail">
                        <img src="<?php echo e(asset('storage/'.$producto->imagen)); ?>"
                             width="200" height="200">
                    </a>
                    <h3><?php echo e($producto->nombre); ?></h3>
                    <p><b>precio: </b><?php echo e($producto->precio); ?></p>
                    <p><b>duracion: </b><?php echo e($producto->dias_duracion); ?></p>
                    <p><b>Cantidad:</b>  <input type="number" class="form-control"  name="cantidad<?php echo e($producto->id); ?>" id="cantidad<?php echo e($producto->id); ?>" min="1" value="1">
                    </p>
                    <p>
                        <button class="btn btn-warning btn-block"
                                onclick="agregarProducto(<?php echo e(json_encode($producto)); ?>,'cantidad<?php echo e($producto->id); ?>',this )">
                            agregar
                        </button>




                    </p>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>

        <div>
            <h3>Detalle de venta</h3>
            <!--         <form action=""> -->
        <!--             <?php echo e(csrf_field()); ?> -->
            <table class="table" id="table">
                <thead>
                <tr>
                    <th>id</th>
                    <th>producto</th>
                    <th>precio</th>
                    <th>cantidad</th>
                    <th>subtotal</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <h1 id="total"></h1>
            <button  class="btn btn-success pull-right" id="send" onclick="vender()">realizar venta</button>
            <!--         </form> -->
        </div>
    </div>





<?php $__env->stopSection(); ?>




D
<script type="text/javascript">
    var total_value = 0;
    var tab;

    function agregarProducto(producto, cantidad_id, btn){
        btn.disabled='disabled';

        var cantidad_value = document.getElementById(cantidad_id).value;

        var table = document.getElementById("table");
        var row = table.insertRow(1);

        var producto_id = row.insertCell(0)
        var nombre = row.insertCell(1);
        var precio = row.insertCell(2);
        var cantidad = row.insertCell(3);
        var subtotal = row.insertCell(4);

        producto_id.innerHTML = producto.id;
        nombre.innerHTML = producto.nombre;
        precio.innerHTML = producto.precio;
        cantidad.innerHTML = cantidad_value;
        subtotal.innerHTML = producto.precio*cantidad_value;

        total_value = total_value+(producto.precio*cantidad_value);

        document.getElementById("total").innerHTML = 'TOTAL Bs. '+total_value;
        //alert(tab);
        tab = $('#table').tableToJSON();
        console.log(tab)

    }


    function vender(){
        $.ajax({
            type: "POST",
            url:"<?php echo e(route('venta_codigos', $cliente->id)); ?>",
            data: {
                table: tab,
                total: total_value
            },
            success: function( msg ) {
                location.href = "<?php echo e(route('voyager.ventas.index')); ?>";
                $("#ajaxResponse").append("<div>"+msg+"</div>");

            }
        });
    }








</script>





<?php echo $__env->make('voyager::master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>