<?php $__env->startSection('page_title', 'Reporte para el admiistrador'); ?>

<?php $__env->startSection('page_header'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <a href="<?php echo e(route('promotor.index')); ?>">
            <div class="col-md-4" style="background: red">Promotores</div>
        </a>
        <a href="<?php echo e(route('user.index')); ?>">
            <div class="col-md-4" style="background: green">Usuarios</div>
        </a>
        <a href="<?php echo e(route('reporte.ventas')); ?>">
            <div class="col-md-4" style="background: #ff9c00">Ventas</div>
        </a>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $(document).ready(
            function () {

            }
        );
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>