<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                    <h1>Detalle de la Venta  <?php echo e($venta->id); ?></h1>


                <div class="panel-body">

                    <p>Fecha:   <?php echo e($venta->fecha); ?></p>
                    <p>Cliente: <?php echo e($venta->cliente); ?></p>
                    <div class="table-responsive">
                        <table class="table table-striped">

                            <thead>
                            <th>producto</th>
                            <th>precio</th>
                            <th>cantidad</th>
                            <th>subtotal</th>
                            </thead>

                            <tbody>
                            <?php $__currentLoopData = $detalles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detalle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($detalle->producto); ?></td>
                                    <td><?php echo e($detalle->precio_actual); ?></td>
                                    <td><?php echo e($detalle->cantidad); ?></td>
                                    <td><?php echo e($detalle->subtotal); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>

                        </table>
                    </div>

                    <h1>Total:  Bs. <?php echo e($venta->total); ?></h1>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('voyager::master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>