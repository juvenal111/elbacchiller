<?php $__env->startSection('page_title', __('voyager::generic.viewing').'Promotores'); ?>

<?php $__env->startSection('page_header'); ?>
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="<?php echo e($dataType->icon); ?>"></i> <?php echo e('Promotores'); ?>

        </h1>
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add',app($dataType->model_name))): ?>
            <a href="<?php echo e(route('promotor.create')); ?>" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span><?php echo e(__('voyager::generic.add_new_promotor')); ?></span>
            </a>
        <?php endif; ?>
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete',app($dataType->model_name))): ?>
            <?php echo $__env->make('voyager::partials.bulk-delete', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit',app($dataType->model_name))): ?>
            <?php if(isset($dataType->order_column) && isset($dataType->order_display_column)): ?>

            <?php endif; ?>
        <?php endif; ?>
        <?php echo $__env->make('voyager::multilingual.language-selector', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content browse container-fluid">
        <?php echo $__env->make('voyager::alerts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <?php if($isServerSide): ?>
                            <form method="get" class="form-search">
                                <div id="search-input">
                                    <select id="search_key" name="key">
                                        <?php $__currentLoopData = $searchable; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($key); ?>" <?php if($search->key == $key): ?><?php echo e('selected'); ?><?php endif; ?>><?php echo e(ucwords(str_replace('_', ' ', $key))); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <select id="filter" name="filter">
                                        <option value="contains" <?php if($search->filter == "contains"): ?><?php echo e('selected'); ?><?php endif; ?>>contains</option>
                                        <option value="equals" <?php if($search->filter == "equals"): ?><?php echo e('selected'); ?><?php endif; ?>>=</option>
                                    </select>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="<?php echo e(__('voyager::generic.search')); ?>" name="s" value="<?php echo e($search->value); ?>">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-lg" type="submit">
                                                <i class="voyager-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        <?php endif; ?>
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete',app($dataType->model_name))): ?>
                                        <th>
                                            <input type="checkbox" class="select_all">
                                        </th>
                                    <?php endif; ?>
                                        <?php 
                                            //dd($dataType->browseRows);


                                                //$numero_identificacion = $dataType->browseRows['11'];

                                                array_forget($dataType->browseRows, '0');
                                                array_forget($dataType->browseRows, '2');
                                                array_forget($dataType->browseRows, '5');
                                                array_forget($dataType->browseRows, '6');
                                                array_forget($dataType->browseRows, '21');
                                                //array_forget($dataType->browseRows, '19');
                                                array_forget($dataType->browseRows, '9');
                                                array_forget($dataType->browseRows, '16');
                                                array_forget($dataType->browseRows, '15');
                                                array_forget($dataType->browseRows, '12');
                                                array_forget($dataType->browseRows, '13');

                                                $row1=$dataType->browseRows['1'];
                                                $dataType->browseRows['1']=$dataType->browseRows['11'];
                                                $dataType->browseRows['11']=$row1;

                                                $row4=$dataType->browseRows['4'];
                                                $dataType->browseRows['4']=$dataType->browseRows['11'];
                                                $dataType->browseRows['11']=$row4;

                                                $row10=$dataType->browseRows['10'];
                                                $row11=$dataType->browseRows['11'];
                                                $row14=$dataType->browseRows['14'];
                                                $dataType->browseRows['10']=$dataType->browseRows['17'];
                                                $dataType->browseRows['11']=$dataType->browseRows['19'];
                                                $dataType->browseRows['14']=$dataType->browseRows['22'];

                                                $dataType->browseRows['17']=$row10;
                                                $dataType->browseRows['19']=$row11;
                                                $dataType->browseRows['22']=$row14;

                                                $dataType->browseRows['17']->field='codigo_enviado';
                                                $dataType->browseRows['17']->display_name='Codigos enviados';
                                                $dataType->browseRows['17']->save();

                                                $dataType->browseRows['22']->field='fecha_envio';
                                                $dataType->browseRows['22']->display_name='Fecha envio';
                                                $dataType->browseRows['22']->save();

                                                $dataType->browseRows['19']->display_name='ID Aplicacion';
                                                $dataType->browseRows['19']->save();

                                                $dataType->browseRows['4']->field='celular';
                                                $dataType->browseRows['4']->save();

                                                //dd($dataType->browseRows);
                                         ?>

                                    <?php $__currentLoopData = $dataType->browseRows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <th>
                                            <?php if($isServerSide): ?>
                                                <a href="<?php echo e($row->sortByUrl()); ?>">
                                                    <?php endif; ?>
                                                    <?php echo e($row->display_name); ?>

                                                    <?php if($isServerSide): ?>
                                                        <?php if($row->isCurrentSortField()): ?>
                                                            <?php if(!isset($_GET['sort_order']) || $_GET['sort_order'] == 'asc'): ?>
                                                                <i class="voyager-angle-up pull-right"></i>
                                                            <?php else: ?>
                                                                <i class="voyager-angle-down pull-right"></i>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                </a>
                                            <?php endif; ?>
                                        </th>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <th class="actions text-right"><?php echo e(__('voyager::generic.actions')); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $dataTypeContent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete',app($dataType->model_name))): ?>
                                            <td>
                                                <input type="checkbox" name="row_id" id="checkbox_<?php echo e($data->getKey()); ?>" value="<?php echo e($data->getKey()); ?>">
                                            </td>
                                        <?php endif; ?>


                                        <?php $__currentLoopData = $dataType->browseRows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                            <td>
                                                <?php $options = json_decode($row->details); ?>
                                                <?php if($row->type == 'image'): ?>
                                                    <img src="<?php if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)): ?><?php echo e(Voyager::image( $data->{$row->field} )); ?><?php else: ?><?php echo e($data->{$row->field}); ?><?php endif; ?>" style="width:100px">
                                                <?php elseif($row->type == 'relationship'): ?>

                                                    <?php if($row->display_name=='ID Aplicacion'): ?>
                                                        <?php echo $__env->make('voyager::formfields.id_enviados', ['view' => 'browse', 'emisor_id' => $data->id], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                    <?php else: ?>
                                                        <?php echo $__env->make('voyager::formfields.relationship', ['view' => 'browse'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                    <?php endif; ?>

                                                <?php elseif($row->type == 'select_multiple'): ?>
                                                    <?php if(property_exists($options, 'relationship')): ?>

                                                        <?php $__currentLoopData = $data->{$row->field}; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                            <?php if($item->{$row->field . '_page_slug'}): ?>
                                                                <a href="<?php echo e($item->{$row->field . '_page_slug'}); ?>"><?php echo e($item->{$row->field}); ?></a><?php if(!$loop->last): ?>, <?php endif; ?>
                                                            <?php else: ?>
                                                                <?php echo e($item->{$row->field}); ?>

                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                        
                                                    <?php elseif(property_exists($options, 'options')): ?>
                                                        <?php $__currentLoopData = $data->{$row->field}; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php echo e($options->options->{$item} . (!$loop->last ? ', ' : '')); ?>

                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>

                                                <?php elseif($row->type == 'select_dropdown' && property_exists($options, 'options')): ?>

                                                    <?php if($data->{$row->field . '_page_slug'}): ?>
                                                        <a href="<?php echo e($data->{$row->field . '_page_slug'}); ?>"><?php echo $options->options->{$data->{$row->field}}; ?></a>
                                                    <?php else: ?>
                                                        <?php echo isset($options->options->{$data->{$row->field}}) ? $options->options->{$data->{$row->field}} : ''; ?>

                                                    <?php endif; ?>


                                                <?php elseif($row->type == 'select_dropdown' && $data->{$row->field . '_page_slug'}): ?>
                                                    <a href="<?php echo e($data->{$row->field . '_page_slug'}); ?>"><?php echo e($data->{$row->field}); ?></a>
                                                <?php elseif($row->type == 'date' || $row->type == 'timestamp'): ?>
                                                        <?php if($row->field==='fecha_envio'): ?>







                                                            <div class="readmore" id="<?php echo e($row->field.''.$data->id); ?>">
                                                                <?php echo e('Elija una ID Aplicacion'); ?>

                                                            </div>

                                                        <?php else: ?>
                                                            <?php echo e($options && property_exists($options, 'format') ? \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($options->format) : $data->{$row->field}); ?>

                                                        <?php endif; ?>
                                                <?php elseif($row->type == 'checkbox'): ?>
                                                    <?php if($options && property_exists($options, 'on') && property_exists($options, 'off')): ?>
                                                        <?php if($data->{$row->field}): ?>
                                                            <span class="label label-info"><?php echo e($options->on); ?></span>
                                                        <?php else: ?>
                                                            <span class="label label-primary"><?php echo e($options->off); ?></span>
                                                        <?php endif; ?>
                                                    <?php else: ?>
                                                        <?php echo e($data->{$row->field}); ?>

                                                    <?php endif; ?>
                                                <?php elseif($row->type == 'color'): ?>
                                                    <span class="badge badge-lg" style="background-color: <?php echo e($data->{$row->field}); ?>"><?php echo e($data->{$row->field}); ?></span>
                                                <?php elseif($row->type == 'text'): ?>
                                                            <?php if($row->field==='codigo_enviado'): ?>
                                                                <?php 
                                                                    $codigos_enviados = \Illuminate\Support\Facades\DB::select(
                                                                        "select COUNT(a.estado) as codigo_enviado
                                                                        from activaciones as a
                                                                        where a.emisor_id=$data->id and a.estado='enviada'
                                                                       ");
                                                                    echo ($codigos_enviados['0']->codigo_enviado) ;
                                                                 ?>

                                                            <?php else: ?>
                                                                    <div class="readmore">
                                                                        <?php echo e(mb_strlen( $data->{$row->field} ) > 200
                                                                            ?
                                                                                mb_substr($data->{$row->field}, 0, 200) . ' ...'
                                                                            :
                                                                                $data->{$row->field}); ?></div>
                                                            <?php endif; ?>

                                                    <?php elseif($row->type == 'number'): ?>
                                                        <?php if($row->field==='codigo_enviado'): ?>
                                                            <?php 
                                                                $codigos_enviados = \Illuminate\Support\Facades\DB::select(
                                                                    "select COUNT(a.estado) as codigo_enviado
                                                                    from activaciones as a
                                                                    where a.emisor_id=$data->id
                                                                   ");
                                                                echo ($codigos_enviados['0']->codigo_enviado) ;
                                                             ?>

                                                        <?php else: ?>
                                                            <div class="readmore">
                                                                <?php echo e(mb_strlen( $data->{$row->field} ) > 200
                                                                    ?
                                                                        mb_substr($data->{$row->field}, 0, 200) . ' ...'
                                                                    :
                                                                        $data->{$row->field}); ?></div>
                                                        <?php endif; ?>

                                                <?php elseif($row->type == 'text_area'): ?>
                                                    <?php echo $__env->make('voyager::multilingual.input-hidden-bread-browse', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                    <div class="readmore"><?php echo e(mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field}); ?></div>
                                                <?php elseif($row->type == 'file' && !empty($data->{$row->field}) ): ?>
                                                    <?php echo $__env->make('voyager::multilingual.input-hidden-bread-browse', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                    <?php if(json_decode($data->{$row->field})): ?>
                                                        <?php $__currentLoopData = json_decode($data->{$row->field}); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <a href="<?php echo e(Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: ''); ?>" target="_blank">
                                                                <?php echo e($file->original_name ?: ''); ?>

                                                            </a>
                                                            <br/>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php else: ?>
                                                        <a href="<?php echo e(Storage::disk(config('voyager.storage.disk'))->url($data->{$row->field})); ?>" target="_blank">
                                                            Download
                                                        </a>
                                                    <?php endif; ?>
                                                <?php elseif($row->type == 'rich_text_box'): ?>
                                                    <?php echo $__env->make('voyager::multilingual.input-hidden-bread-browse', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                    <div class="readmore"><?php echo e(mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>')); ?></div>
                                                <?php elseif($row->type == 'coordinates'): ?>
                                                    <?php echo $__env->make('voyager::partials.coordinates-static-image', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                <?php elseif($row->type == 'multiple_images'): ?>
                                                    <?php  $images = json_decode($data->{$row->field});  ?>
                                                    <?php if($images): ?>
                                                        <?php  $images = array_slice($images, 0, 3);  ?>
                                                        <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <img src="<?php if( !filter_var($image, FILTER_VALIDATE_URL)): ?><?php echo e(Voyager::image( $image )); ?><?php else: ?><?php echo e($image); ?><?php endif; ?>" style="width:50px">
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <?php echo $__env->make('voyager::multilingual.input-hidden-bread-browse', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                    <span><?php echo e($data->{$row->field}); ?></span>
                                                <?php endif; ?>
                                            </td>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <td class="no-sort no-click" id="bread-actions">

                                            <?php $__currentLoopData = Voyager::actions(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $action): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($action=='TCG\Voyager\Actions\EditAction'): ?>
                                                    <?php  $action = new $action($dataType, $data);  ?>
                                                    <a href="<?php echo e(route('promotor.edit', $data->id)); ?>" title="<?php echo e($action->getTitle()); ?>" <?php echo $action->convertAttributesToHtml(); ?>>
                                                        <i class="<?php echo e($action->getIcon()); ?>"></i> <span class="hidden-xs hidden-sm"><?php echo e($action->getTitle()); ?></span>
                                                    </a>
                                                <?php elseif($action=='TCG\Voyager\Actions\ViewAction'): ?>
                                                    <?php  $action = new $action($dataType, $data);  ?>
                                                    <a href="<?php echo e(route('promotor.show', $data->id)); ?>" title="<?php echo e($action->getTitle()); ?>" <?php echo $action->convertAttributesToHtml(); ?>>
                                                        <i class="<?php echo e($action->getIcon()); ?>"></i> <span class="hidden-xs hidden-sm"><?php echo e($action->getTitle()); ?></span>
                                                    </a>
                                                <?php else: ?>
                                                    <?php echo $__env->make('voyager::promotores.partials.actions', ['action' => $action], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <a href="<?php echo e(route('venta-form',$data->id)); ?>" title="<?php echo e('Vender'); ?>"
                                                        class="btn btn-sm btn-success pull-right">
                                                    <i class="voyager-buy"></i> <span class="hidden-xs hidden-sm"><?php echo e('Vender'); ?></span>
                                                </a>

                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                        <?php if($isServerSide): ?>
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite"><?php echo e(trans_choice(
                                    'voyager::generic.showing_entries', $dataTypeContent->total(), [
                                        'from' => $dataTypeContent->firstItem(),
                                        'to' => $dataTypeContent->lastItem(),
                                        'all' => $dataTypeContent->total()
                                    ])); ?></div>
                            </div>
                            <div class="pull-right">
                                <?php echo e($dataTypeContent->appends([
                                    's' => $search->value,
                                    'filter' => $search->filter,
                                    'key' => $search->key,
                                    'order_by' => $orderBy,
                                    'sort_order' => $sortOrder
                                ])->links()); ?>

                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo e(__('voyager::generic.close')); ?>"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> <?php echo e(__('voyager::generic.delete_question')); ?> <?php echo e(strtolower($dataType->display_name_singular)); ?>?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        <?php echo e(method_field("DELETE")); ?>

                        <?php echo e(csrf_field()); ?>

                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="<?php echo e(__('voyager::generic.delete_confirm')); ?>">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><?php echo e(__('voyager::generic.cancel')); ?></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <?php if(!$dataType->server_side && config('dashboard.data_tables.responsive')): ?>
        <link rel="stylesheet" href="<?php echo e(voyager_asset('lib/css/responsive.dataTables.min.css')); ?>">
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <!-- DataTables -->
    <?php if(!$dataType->server_side && config('dashboard.data_tables.responsive')): ?>
        <script src="<?php echo e(voyager_asset('lib/js/dataTables.responsive.min.js')); ?>"></script>
    <?php endif; ?>
    <script>
        var params = {};

        // $("#user_belongsto_role_relationship").on('change', function() {
        //     var value = $(this).val();
        //     var content = $( "#user_belongsto_role_relationship option:selected" ).text();
        //     get_fecha_envio_codigo(value)
        //     //console.log(value);
        // });
        // $('#user_belongsto_role_relationship').on('change', function(){
        //     //var iii = $('#user_belongsto_role_relationship').val();
        //     alert(iii);
        //     console.log(iii)
        //
        //     //$('#input_select_sucursal').val(iii);
        //     params = {
        //         user_id:     iii,
        //     }
        //     //devolver_cajas();
        //
        // });
        function get_fecha_envio_codigo(activacion_id, data_id){
            params = {
                activacion_id:  activacion_id,
                _token: '<?php echo e(csrf_token()); ?>'
            }
            $.post('<?php echo e(route('get_fecha_envio_codigo')); ?>', params, function (response) {
                if ( response
                    && response.data
                    && response.data.status
                    && response.data.status == 200 ) {
                    $("#fecha_envio" + data_id).html(response.data.data.fecha)
                    toastr.success(response.data.message)
                    console.log(response.data)
                } else {
                    console.log(response.data)
                    toastr.error("Error al recuperar fecha de envio codigo")
                }
            });
        }
        // function cambia_contenido(){
        //     $("#capa").html('Nuevo contenido');
        // }



        $(document).ready(function () {
                    <?php if(!$dataType->server_side): ?>
            var table = $('#dataTable').DataTable(<?php echo json_encode(
                    array_merge([
                        "order" => [],
                        "language" => __('voyager::datatable'),
                        "columnDefs" => [['targets' => -1, 'searchable' =>  false, 'orderable' => false]],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true); ?>);
            <?php else: ?>
                $('#search-input select').select2({
                minimumResultsForSearch: Infinity
            });
            <?php endif; ?>

            <?php if($isModelTranslatable): ?>
                $('.side-body').multilingual();
            //Reinitialise the multilingual features when they change tab
            $('#dataTable').on('draw.dt', function(){
                $('.side-body').data('multilingual').init();
            })
            <?php endif; ?>
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '<?php echo e(route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id'])); ?>'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>