<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// login
Route::get('get-paises','PaisController@index');
Route::get('get-ciudades/{pais_id}','CiudadController@getCiudades');
Route::post('login-google','ApiLoginController@loginGoogle');
Route::post('login-facebook','ApiLoginController@loginFacebook');
Route::post('registrar-celular/{user_id}','ApiLoginController@registrarCelular');

// orientacion
Route::get('get-areas/{nivel}','AreaController@getAreas');
Route::get('get-perfil-carreras/{area_id}','PerfilCarreraController@getPerfilCarreras');
Route::get('get-perfil-carrera/{perfil_carrera_id}','PerfilCarreraController@gePerfilCarrera');

// principal
Route::get('get-instituciones/{ciudad_id}/{tipo}','InstitucionController@getInstituciones');
Route::get('get-institucion/{institucion_id}','InstitucionController@getInstitucion');
Route::get('get-carreras/{institucion_id}','CarreraController@getCarreras');
Route::get('get-lineas/{institucion_id}','LineaController@getLineas');
Route::get('get-general/{ciudad_id}/{tipo}','CarreraController@getGeneral');

//activaciones
Route::get('get-puntos-compra/{ciudad_id}','PuntoCompraController@getPuntosDeCompras');
Route::get('get-stock/{user_id}','ActivacionController@getStock');
Route::post('transferir-activacion/{user_id}/{producto_id}','ActivacionController@transferirActivacion');
Route::get('get-activacion/{user_id}','ActivacionController@getActivacion');
Route::get('activar/{activacion_id}','ActivacionController@activar');
Route::get('get-historial/{user_id}','ActivacionController@getHistorial');


// PSA
Route::get('get-psa/{ciudad_id}','PsaController@getPsa');
Route::get('get-psa-gestiones/{ciudades_id}','PsaController@getGestionesPsa');
Route::get('get-psa-areas/{psa_gestion_id}','PsaController@getAreasPsa');
Route::get('get-psa-textos/{psa_gestion_id}','PsaController@getTextosPsa');
Route::get('iniciar-psa/{psa_gestion_id}/{psa_area_id}','PsaController@darPsa');







