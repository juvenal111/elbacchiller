<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//     return view('welcome');
       return redirect()->route('voyager.dashboard');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
  
   Route::get('venta-form/{user_id}','VentaController@ventaForm')->name('venta-form');
   Route::post('vender/{user_id}','VentaController@ventaCodigos')->name('venta_codigos');
   Route::get('ver-ventas','VentaController@verVentas')->name('ver-ventas');
   Route::get('ver-detalle/{venta_id}','VentaController@verDetalle')->name('ver-detalle');
  
  
  
  Route::get('ver-area-materias/{area_id}','PsaAreaMateriaController@verAreaMaterias')->name('ver-area-materias');
  Route::post('editar-area-materia/{area_materia_id}','PsaAreaMateriaController@editarAreaMateria')->name('editar-area-materia');
  
  Route::get('ver-preguntas/{area_id}/{materia_id}','PsaPreguntaController@verPreguntas')->name('ver-preguntas');
  Route::post('guardar-pregunta/{area_materia_id}','PsaPreguntaController@guardarPregunta')->name('guardar-pregunta');
  Route::post('editar-pregunta/{pregunta_id}','PsaPreguntaController@editarPregunta')->name('editar-pregunta');
  Route::get('eliminar-pregunta/{pregunta_id}','PsaPreguntaController@eliminarPregunta')->name('eliminar-pregunta');

  Route::get('ver-respuestas/{pregunta_id}','PsaRespuestaController@verRespuestas')->name('ver-respuestas');
  Route::post('guardar-respuesta/{pregunta_id}','PsaRespuestaController@guardarRespuesta')->name('guardar-respuesta');
  Route::post('editar-respuesta/{respuesta_id}','PsaRespuestaController@editarRespuesta')->name('editar-respuesta');
  Route::get('eliminar-respuesta/{respuesta_id}','PsaRespuestaController@eliminarRespuesta')->name('eliminar-respuesta');
  
  Route::get('ver-punto-compras/{ciudad_id}','PuntoCompraController@verPuntosDeCompras')->name('ver-punto-compras');





  Route::get('ver-gestiones-psa/{psa_id}','GestionPsaController@verGestionesPsa')->name('ver-gestiones-psa');
  Route::post('guardar-gestion-psa/{psa_id}','GestionPsaController@guardarGestionPsa')->name('guardar-gestion-psa');
  Route::post('editar-gestion-psa/{gestion_id}','GestionPsaController@editarGestionPsa')->name('editar-gestion-psa');
  Route::get('eliminar-gestion-psa/{gestion_id}','GestionPsaController@eliminarGestionPsa')->name('eliminar-gestion-psa');

  Route::get('ver-materias-psa/{gestion_id}','MateriaPsaController@verMateriasPsa')->name('ver-materias-psa');
  Route::post('guardar-materia-psa/{gestion_id}','MateriaPsaController@guardarMateriaPsa')->name('guardar-materia-psa');
  Route::post('editar-materia-psa/{materia_id}','MateriaPsaController@editarMateriaPsa')->name('editar-materia-psa');
  Route::get('eliminar-materia-psa/{materia_id}','MateriaPsaController@eliminarMateriaPsa')->name('eliminar-materia-psa');

  Route::get('ver-areas-psa/{gestion_id}','AreaPsaController@verAreasPsa')->name('ver-areas-psa');
  Route::post('guardar-area-psa/{gestion_id}','AreaPsaController@guardarAreaPsa')->name('guardar-area-psa');
  Route::post('editar-area-psa/{area_id}','AreaPsaController@editarAreaPsa')->name('editar-area-psa');
  Route::get('eliminar-area-psa/{area_id}','AreaPsaController@eliminarAreaPsa')->name('eliminar-area-psa');

  Route::get('ver-preguntas-psa/{materia_id}','PreguntaPsaController@verPreguntasPsa')->name('ver-preguntas-psa');
  Route::post('guardar-pregunta-psa/{materia_id}','PreguntaPsaController@guardarPreguntaPsa')->name('guardar-pregunta-psa');
  Route::post('editar-pregunta-psa/{pregunta_id}','PreguntaPsaController@editarPreguntaPsa')->name('editar-pregunta-psa');
  Route::get('eliminar-pregunta-psa/{pregunta_id}','PreguntaPsaController@eliminarPreguntaPsa')->name('eliminar-pregunta-psa');

  Route::get('ver-respuestas-psa/{pregunta_id}','RespuestaPsaController@verRespuestasPsa')->name('ver-respuestas-psa');
  Route::post('guardar-respuesta-psa/{pregunta_id}','RespuestaPsaController@guardarRespuestaPsa')->name('guardar-respuesta-psa');
  Route::post('editar-respuesta-psa/{respuesta_id}','RespuestaPsaController@editarRespuestaPsa')->name('editar-respuesta-psa');
  Route::get('eliminar-respuesta-psa/{respuesta_id}','RespuestaPsaController@eliminarRespuestaPsa')->name('eliminar-respuesta-psa');

  // AreaMaterias
  Route::get('ver-area-materias-psa/{area_id}','AreaPsaController@verAreaMateriasPsa')->name('ver-area-materias-psa');
  Route::post('agregar-materia-psa/{area_id}','AreaPsaController@agregarMateriaPsa')->name('agregar-materia-psa');
  Route::get('quitar-materia-psa/{materia_id}/{area_id}','AreaPsaController@quitarMateriaPsa')->name('quitar-materia-psa');

  //**************************JUVENAL***********************
    // reporte administración
    Route::get('reporte_administracion/','ReporteAdministracionController@index')->name('reporte_administracion');

    //promotores
    Route::get('reporte_administracion/promotores','PromotorController@index')->name('promotor.index');
    Route::get('reporte_administracion/promotores/edit/{user_id}','PromotorController@edit')->name('promotor.edit');
    Route::post('reporte_administracion/promotores/update/{user_id}','PromotorController@update')->name('promotor.update');
    Route::get('reporte_administracion/promotores/create','PromotorController@create')->name('promotor.create');
    Route::post('reporte_administracion/promotores/store','PromotorController@store_promotor')->name('promotor.store');
    Route::get('reporte_administracion/promotores/show/{user_id}','PromotorController@show')->name('promotor.show');

  //usuarios
  Route::get('usuarios','UserController@index')->name('user.index');

Route::post('get_tipo_usuario','AjaxController@get_tipo_usuario')
    ->name('get_tipo_usuario');

  //ventas
  Route::get('reporte/ventas','VentaController@index')->name('reporte.ventas');
  Route::post('get_ventas','AjaxController@getVentas')->name('get_ventas');
    Route::post('get_ventas_departamento','AjaxController@getVentasDepartamento')->name('get_ventas_departamento');

    //ajax*********
    //user
    Route::get('get_user','UserController@getUser')->name('get_user');

//    ubicacion dinamica al crear promotor
    Route::get('get_departamentos','AjaxController@get_departamentos')->name('get_departamentos');
    Route::get('get_provincias','AjaxController@get_provincias')->name('get_provincias');
    Route::get('get_municipios','AjaxController@get_municipios')->name('get_municipios');
    Route::post('get_fecha_envio_codigo','AjaxController@get_fecha_envio_codigo')->name('get_fecha_envio_codigo');

  Route::post('historial_codigos','AjaxController@historial_codigos')->name('historial_codigos');
});



