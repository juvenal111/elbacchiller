<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Venta extends Model
{
      protected $table = 'ventas';
    public function user_cliente(){
        return $this->belongsTo('App\User','cliente_id');
    }
}
