<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerfilCarrera extends Model
{
    protected $table = 'perfil_carreras';
}
