<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PsaPregunta extends Model
{
    protected $table = 'psa_preguntas';
}
