<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PsaArea extends Model
{
    protected $table = 'psa_areas';
}
