<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\PsaPregunta;
use App\PsaRespuesta;
use Alert;
use Illuminate\Support\Facades\Storage;

class PsaRespuestaController extends Controller
{
  
  public function verRespuestas($pregunta_id){
    $respuestas = DB::table('psa_respuestas as r')
      ->where('r.psa_pregunta_id',$pregunta_id)
      ->orderby('texto')
      ->get();
    $pregunta = DB::table('psa_preguntas as p')
        ->where('p.id',$pregunta_id)
        ->first();
    return view('psa.respuestas.index',['respuestas'=>$respuestas, 'pregunta'=>$pregunta]);  
  } 
  
  public function guardarRespuesta(Request $request,$pregunta_id){  
      if($request->imagen){
          $file = $request->imagen;
          $name = $file->hashName();
          $content =  file_get_contents($file -> getRealPath()) ;
          Storage::disk('respuestas')->put($name,$content);
        
          $respuesta = new PsaRespuesta();
          $respuesta->texto = $request->texto;
          $respuesta->imagen = 'respuestas/'.$name;
          $respuesta->psa_pregunta_id = $pregunta_id;
          $respuesta->tipo = $request->tipo;
          $respuesta->save();
      }else{
          $respuesta = new PsaRespuesta();
          $respuesta->texto = $request->texto;
          $respuesta->psa_pregunta_id = $pregunta_id;
          $respuesta->tipo = $request->tipo;
          $respuesta->save();
      }
//     Alert::success('Respuesta registrada correctamente','Respuesta');
    return redirect()->back();    
  }  
  
  public function editarRespuesta(Request $request,$respuesta_id){  
      if($request->imagen){
          $file = $request->imagen;
          $name = $file->hashName();
          $content =  file_get_contents($file -> getRealPath()) ;
          Storage::disk('respuestas')->put($name,$content);
        
          $respuesta = PsaRespuesta::find($respuesta_id);
          $respuesta->texto = $request->texto;
          $respuesta->imagen = 'respuestas/'.$name;
          $respuesta->tipo = $request->tipo;
          $respuesta->save();
      }else{
          $respuesta = PsaRespuesta::find($respuesta_id);
          $respuesta->texto = $request->texto;
          $respuesta->tipo = $request->tipo;
          $respuesta->save();
      }
//         Alert::success('Respuesta editada correctamente','Respuesta');
       return redirect()->back();    
  }
  
  public function eliminarRespuesta($respuesta_id){
    $respuesta = PsaRespuesta::find($respuesta_id);
    $respuesta->delete();
//     Alert::success('Respuesta eliminada correctamente','Respuesta');
    return redirect()->back(); 
  }


}
