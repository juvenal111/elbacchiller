<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PaisController extends Controller
{
    public function index(){
      $paises = DB::table('paises as p')
        ->where('p.estado',1)
        ->get();
      $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
      if($paises->isNotEmpty()){
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'lista de paises',
              'datos' => $paises
          ],200);
      }else{
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'no hay paises registradas',
              'datos' => null
          ],404);
      }
    }

   
}
