<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\MateriaPsa;
use App\AreaPsa;
use App\AreaMateriaPsa;

class AreaPsaController extends Controller
{
    public function verAreasPsa($gestion_id)
    {
        $areas  =  DB::table('areas_psa as a')
            ->where('a.gestion_psa_id',$gestion_id)
            ->get();

        $gestion = DB::table('gestiones_psa as g')
            ->select('g.id','g.nombre')
            ->where('g.id',$gestion_id)
            ->first();
        return view('psa2.areas_psa.index',['areas' => $areas , 'gestion' => $gestion]);
    }

    public function guardarAreaPsa(Request $request,$gestion_id)
    {
        $area = new AreaPsa();
        $area->nombre = $request->nombre;
        $area->gestion_psa_id = $gestion_id;
        $area->save();
        return redirect()->back();
    }

    public function editarAreaPsa(Request $request,$area_id){
        $area = AreaPsa::find($area_id);
        $area->nombre = $request->nombre;
        $area->save();
        return redirect()->back();
    }

    public function eliminarAreaPsa($area_id)
    {
        $area = AreaPsa::find($area_id);
        $area->delete();
        return redirect()->back();
    }










    
    public function verAreaMateriasPsa($area_id)
    {
        $materias  =  DB::table('area_materias_psa as am')
            ->select('m.id','m.nombre')
            ->join('materias_psa as m','am.materia_psa_id','m.id')
            ->where('am.area_psa_id',$area_id)
            ->get();

        $gestion = DB::table('gestiones_psa as g')
            ->select('g.id')
            ->join('areas_psa as a','a.gestion_psa_id','g.id')
            ->where('a.id',$area_id)
            ->first();

        $todas_materias  =  DB::table('materias_psa as m')
            ->where('m.gestion_psa_id',$gestion->id)
            ->get();

        $area = DB::table('areas_psa as a')
            ->select('a.id','a.nombre')
            ->where('a.id',$area_id)
            ->first();
        return view('psa2.areas_psa.add-materia',['materias' => $materias , 'area' => $area,'todas_materias' =>$todas_materias]);
    }

    public function agregarMateriaPsa(Request $request,$area_id)
    {
        $area_materia = new AreaMateriaPsa();
        $area_materia->area_psa_id = $area_id;
        $area_materia->materia_psa_id = $request->materia;
        $area_materia->save();
        return redirect()->back();
    }

    public function quitarMateriaPsa($materia_id,$area_id)
    {
        $area_materia_aux = DB::table('area_materias_psa as am')
            ->select('am.id')
            ->where('am.area_psa_id',$area_id)
            ->where('am.materia_psa_id',$materia_id)
            ->first();

        $area_materia = AreaMateriaPsa::find($area_materia_aux->id);
        $area_materia->delete();
        return redirect()->back();
    }

}
