<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\PsaPregunta;
use App\PsaRespuesta;
use Alert;
use Illuminate\Support\Facades\Storage;

class PsaPreguntaController extends Controller
{

   public function verPreguntas($area_id,$materia_id){
      $search = \Request::get('search');  
      $area_materia = DB::table('psa_area_materias as am')
        ->where('am.psa_area_id',$area_id)
        ->where('am.psa_materia_id',$materia_id)
        ->first();
      
      $preguntas = DB::table('psa_preguntas as p')
        ->select('p.id','p.texto','p.imagen','p.texto2')
        ->join('psa_area_materias as am','am.id','p.psa_area_materias_id')
        ->where('p.psa_area_materias_id',$area_materia->id)     
        ->where('p.id','like','%'.$search.'%')
        ->orderBy('p.id','desc')
        ->paginate(10);

      $area = DB::table('psa_areas as a')
        ->select('a.id','a.nombre','g.nombre as nombre_gestion')
        ->join('psa_gestiones as g','g.id','a.psa_gestion_id')
        ->where('a.id',$area_id)
        ->first();
      
      $materia = DB::table('psa_materias as m')
       ->where('m.id',$materia_id)
        ->first();
     
      return view('psa.preguntas.index',['preguntas' => $preguntas,
                                         'area_materia' => $area_materia,
                                         'area'=>$area,
                                         'materia'=>$materia]);  
  }
    
  public function guardarPregunta(Request $request,$area_materia_id){
      if($request->imagen){
          $file = $request->imagen;
          $name = $file->hashName();
          $content =  file_get_contents($file -> getRealPath()) ;
          Storage::disk('preguntas')->put($name,$content);
        
          $pregunta = new PsaPregunta();
          $pregunta->texto = $request->texto;
          $pregunta->texto2 = $request->texto2;
          $pregunta->imagen = 'preguntas/'.$name;
          $pregunta->psa_area_materias_id = $area_materia_id;
          $pregunta->save();
      }else{
          $pregunta = new PsaPregunta();
          $pregunta->texto = $request->texto;
          $pregunta->texto2 = $request->texto2;
          $pregunta->psa_area_materias_id = $area_materia_id;
          $pregunta->save();
      }
//     Alert::success('Pregunta registrada correctamente','Pregunta');
    return redirect()->back();
  }
  
  public function editarPregunta(Request $request,$pregunta_id){
      if($request->imagen){
          $file = $request->imagen;
          $name = $file->hashName();
          $content =  file_get_contents($file -> getRealPath()) ;
          Storage::disk('preguntas')->put($name,$content);
        
          $pregunta = PsaPregunta::find($pregunta_id);
          $pregunta->texto = $request->texto;
          $pregunta->texto2 = $request->texto2;
          $pregunta->imagen = 'preguntas/'.$name;
          $pregunta->save();
      }else{
          $pregunta = PsaPregunta::find($pregunta_id);
          $pregunta->texto = $request->texto;
          $pregunta->texto2 = $request->texto2;
          $pregunta->save();
      }
//     Alert::success('Pregunta editada correctamente','Pregunta');
    return redirect()->back();
  }
  
  public function eliminarPregunta($pregunta_id){
    DB::table('psa_respuestas')
      ->where('psa_pregunta_id',$pregunta_id)
      ->delete();
    $psa_pregunta = PsaPregunta::find($pregunta_id);
    $psa_pregunta->delete();
//     Alert::success('Pregunta eliminada correctamente','Pregunta');
    return redirect()->back();
  }
  


}
