<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReporteAdministracionController extends Controller
{
    public function index(Request $request)
    {
        $dataType = '';
        return view('reporte_administracion.index', compact(
            'dataType'
        ));
    }
}
