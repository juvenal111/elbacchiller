<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\MateriaPsa;

class MateriaPsaController extends Controller
{

    public function verMateriasPsa($gestion_id)
    {
        $materias  =  DB::table('materias_psa as m')
            ->where('m.gestion_psa_id',$gestion_id)
            ->get();

        $gestion = DB::table('gestiones_psa as g')
            ->select('g.id','g.nombre')
            ->where('g.id',$gestion_id)
            ->first();
        return view('psa2.materias_psa.index',['materias' => $materias , 'gestion' => $gestion]);
    }

    public function guardarMateriaPsa(Request $request,$gestion_id)
    {
        $materia = new MateriaPsa();
        $materia->nombre = $request->nombre;
        $materia->gestion_psa_id = $gestion_id;
        $materia->link_texto = $request->link_texto;
        $materia->save();
        return redirect()->back();
    }

    public function editarMateriaPsa(Request $request,$materia_id)
    {
        $materia = MateriaPsa::find($materia_id);
        $materia->nombre = $request->nombre;
        $materia->link_texto = $request->link_texto;
        $materia->save();
        return redirect()->back();
    }

    public function eliminarMateriaPsa($materia_id)
    {
        $materia = MateriaPsa::find($materia_id);
        $materia->delete();
        return redirect()->back();
    }


}
