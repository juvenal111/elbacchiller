<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\PreguntaPsa;
use App\RespuestaPsa;
use Illuminate\Support\Facades\Storage;


class PreguntaPsaController extends Controller
{
    public function verPreguntasPsa($materia_id){
        $search = \Request::get('search');

        $preguntas = DB::table('preguntas_psa as p')
            ->select('p.id','p.texto','p.imagen','p.texto2')
            ->where('p.materia_psa_id',$materia_id)
            ->where('p.id','like','%'.$search.'%')
            ->orderBy('p.id','desc')
            ->paginate(10);

        $materia = DB::table('materias_psa as m')
            ->select('m.id','m.nombre','m.link_texto','g.nombre as nombre_gestion')
            ->join('gestiones_psa as g','g.id','m.gestion_psa_id')
            ->where('m.id',$materia_id)
            ->first();

        return view('psa2.preguntas_psa.index',
            ['preguntas' => $preguntas,
            'materia'=>$materia]);
    }

    public function guardarPreguntaPsa(Request $request,$materia_id){
        if($request->imagen){
            $file = $request->imagen;
            $name = $file->hashName();
            $content =  file_get_contents($file -> getRealPath()) ;
            Storage::disk('preguntas')->put($name,$content);

            $pregunta = new PreguntaPsa();
            $pregunta->texto = $request->texto;
            $pregunta->texto2 = $request->texto2;
            $pregunta->imagen = 'preguntas/'.$name;
            $pregunta->materia_psa_id = $materia_id;
            $pregunta->save();
        }else{
            $pregunta = new PreguntaPsa();
            $pregunta->texto = $request->texto;
            $pregunta->texto2 = $request->texto2;
            $pregunta->materia_psa_id = $materia_id;
            $pregunta->save();
        }
        return redirect()->back();
    }
//
    public function editarPreguntaPsa(Request $request,$pregunta_id){
        if($request->imagen){
            $file = $request->imagen;
            $name = $file->hashName();
            $content =  file_get_contents($file -> getRealPath()) ;
            Storage::disk('preguntas')->put($name,$content);

            $pregunta = PreguntaPsa::find($pregunta_id);
            $pregunta->texto = $request->texto;
            $pregunta->texto2 = $request->texto2;
            $pregunta->imagen = 'preguntas/'.$name;
            $pregunta->save();
        }else{
            $pregunta = PreguntaPsa::find($pregunta_id);
            $pregunta->texto = $request->texto;
            $pregunta->texto2 = $request->texto2;
            $pregunta->save();
        }
        return redirect()->back();
    }

    public function eliminarPreguntaPsa($pregunta_id)
    {
        $pregunta = PreguntaPsa::find($pregunta_id);
        $pregunta->delete();
        return redirect()->back();
    }

}
