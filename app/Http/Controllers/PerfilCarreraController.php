<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PerfilCarreraController extends Controller
{
    public function getPerfilCarreras($area_id){
      $carreras = DB::table('perfil_carreras as pc')
        ->where('pc.area_id',$area_id)
        ->where('pc.estado',1)
        ->get();
      $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
      if($carreras->isNotEmpty()){
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'lista de perfil-carreras',
              'datos' => $carreras
          ],200);
      }else{
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'no hay perfil-carreras registradas',
              'datos' => null
          ],404);
      }
  }
  
  public function gePerfilCarrera($perfil_carrera_id){
      $carrera = DB::table('perfil_carreras as pc')
        ->where('pc.id',$perfil_carrera_id)
        ->first();
      $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
      if($carrera){
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'informacion de la perfil-carrera',
              'datos' => $carrera
          ],200);
      }else{
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'no se encontro una perfil-carrera con ese id',
              'datos' => null
          ],404);
      }
  }
}
