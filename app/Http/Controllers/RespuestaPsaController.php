<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\PreguntaPsa;
use App\RespuestaPsa;
use Illuminate\Http\Request;

class RespuestaPsaController extends Controller
{
    public function verRespuestasPsa($pregunta_id){
        $respuestas = DB::table('respuestas_psa as r')
            ->where('r.pregunta_psa_id',$pregunta_id)
            ->orderby('texto')
            ->get();
        $pregunta = DB::table('preguntas_psa as p')
            ->where('p.id',$pregunta_id)
            ->first();
        return view('psa2.respuestas_psa.index',['respuestas'=>$respuestas, 'pregunta'=>$pregunta]);
    }

    public function guardarRespuestaPsa(Request $request,$pregunta_id){
        if($request->imagen){
            $file = $request->imagen;
            $name = $file->hashName();
            $content =  file_get_contents($file -> getRealPath()) ;
            Storage::disk('respuestas')->put($name,$content);

            $respuesta = new RespuestaPsa();
            $respuesta->texto = $request->texto;
            $respuesta->imagen = 'respuestas/'.$name;
            $respuesta->pregunta_psa_id = $pregunta_id;
            $respuesta->tipo = $request->tipo;
            $respuesta->save();
        }else{
            $respuesta = new RespuestaPsa();
            $respuesta->texto = $request->texto;
            $respuesta->pregunta_psa_id = $pregunta_id;
            $respuesta->tipo = $request->tipo;
            $respuesta->save();
        }
        return redirect()->back();
    }

    public function editarRespuestaPsa(Request $request,$respuesta_id){
        if($request->imagen){
            $file = $request->imagen;
            $name = $file->hashName();
            $content =  file_get_contents($file -> getRealPath()) ;
            Storage::disk('respuestas')->put($name,$content);

            $respuesta = RespuestaPsa::find($respuesta_id);
            $respuesta->texto = $request->texto;
            $respuesta->imagen = 'respuestas/'.$name;
            $respuesta->tipo = $request->tipo;
            $respuesta->save();
        }else{
            $respuesta = RespuestaPsa::find($respuesta_id);
            $respuesta->texto = $request->texto;
            $respuesta->tipo = $request->tipo;
            $respuesta->save();
        }
        return redirect()->back();
    }

    public function eliminarRespuestaPsa($respuesta_id){
        $respuesta = RespuestaPsa::find($respuesta_id);
        $respuesta->delete();
        return redirect()->back();
    }
}
