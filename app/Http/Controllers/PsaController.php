<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class PsaController extends Controller
{

    public function getPsa($ciduad_id){
      $psa = DB::table('psas as p')
        ->where('p.ciudad_id',$ciduad_id)
        ->first();
      $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
      if($psa){
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'psa',
              'datos' => $psa
          ],200);
      }else{
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'no hay psa',
              'datos' => null
          ],404);
      }
  }

    public function getGestionesPsa($ciudad_id){
        $gestiones = DB::table('gestiones_psa as g')
            ->select('g.id','g.nombre')
            ->join('psas as p','p.id','g.psa_id')
            ->where('p.ciudad_id',$ciudad_id)
            ->get();

        $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
        if($gestiones){
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'lista de gestiones del PSA',
                'datos' => $gestiones
            ],200);
        }else{
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'no hay gestiones del PSA registradas',
                'datos' => null
            ],404);
        }
    }

    public function getAreasPsa($gestion_id){
        $areas = DB::table('areas_psa as a')
            ->select('a.id','a.nombre')
            ->where('a.gestion_psa_id',$gestion_id)
            ->get();
        $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
        if($areas){
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'lista de areas del PSA de la gestion con ID:'.$gestion_id,
                'datos' => $areas
            ],200);
        }else{
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'no hay areas del PSA registradas de la gestion con ID:'.$gestion_id,
                'datos' => null
            ],404);
        }
    }

    public function getTextosPsa($gestion_id)
    {
        $areas = DB::table('areas_psa as a')
            ->where('a.gestion_psa_id', $gestion_id)
            ->orderBy('id', 'desc')
            ->get();

        $array_areas = array();
        foreach ($areas as $area) {
            $elemento = array('id' => $area->id, 'nombre' => $area->nombre);
            $array_areas = array_prepend($array_areas, $elemento);
        }


        for ($i = 0; $i < count($array_areas); $i++) {
            $materias = DB::table('materias_psa as m')
                ->select('m.nombre', 'm.link_texto')
                ->join('area_materias_psa as am', 'am.materia_psa_id', 'm.id')
                ->where('am.area_psa_id', $array_areas[$i]['id'])
                ->get();

            $array_materias = array();
            foreach ($materias as $materia) {
                $elemento = array('nombre' => $materia->nombre, 'link_texto' => $materia->link_texto);
                $array_materias = array_prepend($array_materias, $elemento);
            }
            $array_areas[$i] = array_add($array_areas[$i], 'textos', $array_materias);//ok
        }

        $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
        if($array_areas){
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'textos del psa',
                'datos' => $array_areas
            ],200);
        }else{
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'no hay textos',
                'datos' => null
            ],404);
        }
    }

    public function darPsa($gestion_id,$area_id){
        $materias = DB::table('materias_psa as m')
            ->select('m.id','m.nombre')
            ->join('area_materias_psa as am','am.materia_psa_id','m.id')
            ->where('m.gestion_psa_id',$gestion_id)
            ->where('am.area_psa_id',$area_id)
            ->orderBy('id','desc')
            ->get();

        $array_materias = array();
        foreach($materias as $materia){
            $mat = array('id' => $materia->id, 'nombre' => $materia->nombre);
            $array_materias = array_prepend($array_materias,$mat);
        }

        for($i =0;$i < count($array_materias);$i++){
            $preguntas = DB::table('preguntas_psa as p')
                ->select('p.id','p.texto','p.imagen','p.texto2')
                ->where('p.materia_psa_id',$array_materias[$i]['id'])
                ->inRandomOrder()
                ->take(10)
                ->get();

            $array_preguntas = array();
            foreach($preguntas as $pregunta){
                $pre = array('id'=>$pregunta->id,'texto'=>$pregunta->texto,'imagen'=>$pregunta->imagen,'texto2'=> $pregunta->texto2);
                $array_preguntas = array_prepend($array_preguntas,$pre);
            }

            $array_materias[$i] = array_add( $array_materias[$i],'preguntas',$array_preguntas);//ok

            for($j =0;$j < count($array_preguntas);$j++){
                $respuestas = DB::table('respuestas_psa as r')
                    ->select('r.id','r.texto','r.imagen','r.tipo')
                    ->where('r.pregunta_psa_id',$array_preguntas[$j]['id'])
                    ->orderBy('id','desc')
                    ->get();


                $array_respuestas = array();
                foreach($respuestas as $respuesta){
                    $res = array('id'=>$respuesta->id,'texto'=>$respuesta->texto,'imagen'=>$respuesta->imagen,'tipo'=>$respuesta->tipo);
                    $array_respuestas = array_prepend($array_respuestas,$res);
                }
                $array_materias[$i]['preguntas'][$j] = array_add(  $array_materias[$i]['preguntas'][$j],'respuestas',$array_respuestas);
            }
        }

        $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
        if($materias->isNotEmpty()){
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'lista de materias,preguntas y respuestas del PSA del area con ID:'.$area_id,
                'datos' => $array_materias
            ],200);
        }else{
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'no hay materias,preguntas y respuestas del PSA registradas del area con ID:'.$area_id,
                'datos' => null
            ],404);
        }
    }

  
}
