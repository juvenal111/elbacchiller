<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class LineaController extends Controller
{
  public function getLineas($institucion_id){
      $lineas = DB::table('lineas as l')
        ->select('l.nombre','l.tipo','l.color')
        ->join('institucion_lineas as il','il.linea_id','l.id')
        ->where('il.institucion_id',$institucion_id)
        ->where('l.estado',1)
        ->get();
      $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
      if($lineas){
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'lista de lineas',
              'datos' => $lineas
          ],200);
      }else{
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'no hay lineas registradas',
              'datos' => null
          ],404);
      }
  }
}
