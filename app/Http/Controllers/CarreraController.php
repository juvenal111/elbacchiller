<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CarreraController extends Controller
{
    public function getCarreras($institucion_id){
      $carreras = DB::table('carreras as c')
      ->where('c.institucion_id',$institucion_id)
      ->where('c.estado',1)
      ->get();
      $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
      if($carreras->isNotEmpty()){
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'lista de carreras',
              'datos' => $carreras
          ],200);
      }else{
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'no hay carreras registradas',
              'datos' => null
          ],404);
      }
  }
  
 public function getGeneral($ciudad_id,$tipo){
    $carreras = DB::table('carreras as c')
      ->select('c.id','c.nombre','c.costo_inscripcion','c.costo_matricula','c.institucion_id')
      ->join('instituciones as i','i.id','c.institucion_id')
      ->where('i.ciudad_id',$ciudad_id)
      ->where('i.tipo',$tipo)
      ->where('c.estado',1)     
      ->get();
    $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
    if( $carreras->isNotEmpty()){
      switch ($tipo) {
          case 'universidad':
              return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'lista de carreras de las universidades',
              'datos' => $carreras
              ],200);
              break;
          case 'instituto':
              return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'lista de carreras de los institutos',
              'datos' => $carreras
              ],200);
              break;
          case 'zona_de_aprendisaje':
              return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'lista de carreras de las zonas de aprendisaje',
              'datos' => $carreras
              ],200);
              break;
      }
      
    }else{
        return response()->json([
            'fecha' => $fecha,
            'mensaje' => 'no hay instituciones registradas',
            'datos' => null
        ],404);
    }
}

  



}
