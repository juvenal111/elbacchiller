<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AreaController extends Controller
{

  public function getAreas($nivel){
      $areas = DB::table('areas as a')
        ->where('a.nivel',$nivel)
        ->where('a.estado',1)
        ->get();
      $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
      if($areas->isNotEmpty()){
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'lista de areas',
              'datos' => $areas
          ],200);
      }else{
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'no hay areas registradas',
              'datos' => null
          ],404);
      }
  }
  
}
