<?php

namespace App\Http\Controllers;

use App\Activacion;
use App\Departamento;
use App\DetalleVenta;
use App\DetalleVentaRespaldo;
use App\Municipio;
use App\Provincia;
use App\Venta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;

class AjaxController extends Controller
{

    public function get_departamentos(Request $request){
        $departamentos=Departamento::where('pais_id', '=', $request->pais_id)->get();
        return response()->json([
            'data' => [
                'id'  => 200,
                'message' => 'exito',
                'data'    => $departamentos,
            ],
        ]);

        //return response()->$departamentos;

    }

    public function getVentas(Request $request){
        $ventas = DB::select("
        select sum(v.total) as total, p.nombre as pais, d.nombre as departamento
FROM ventas as v, users as u, paises as p, departamentos as d
where v.cliente_id=u.id and u.Pais=p.id and u.departamento_id=d.id and p.id=$request->pais_id
group by p.nombre, d.nombre
        ");



        return response()->json([
            'data' => [
                'status'  => 200,
                'message' => 'exito',
                'data'    => $ventas,
            ],
        ]);

        //return response()->$departamentos;

    }

    public function getVentasDepartamento(Request $request){
        $ventas = DB::select("
        select sum(v.total) as total, p.nombre as pais, d.nombre as departamento
FROM ventas as v, users as u, paises as p, departamentos as d
where v.cliente_id=u.id and u.Pais=p.id and u.departamento_id=d.id and d.id=$request->departamento_id
group by p.nombre, d.nombre
        ");



        return response()->json([
            'data' => [
                'status'  => 200,
                'message' => 'exito',
                'data'    => $ventas,
            ],
        ]);

        //return response()->$departamentos;

    }



    public function get_provincias(Request $request){

        $provincias = Provincia::where('departamento_id', '=', $request->departamento_id)->get();
        return response()->json([
            'data' => [
                'id'  => 200,
                'message' => 'exito',
                'data'    => $provincias,
            ],
        ]);

        //return response()->$departamentos;

    }

    public function get_municipios(Request $request){

        $municipios = Municipio::where('provincia_id', '=', $request->provincia_id)->get();
        return response()->json([
            'data' => [
                'id'  => 200,
                'message' => 'exito',
                'data'    => $municipios,
            ],
        ]);

        //return response()->$departamentos;

    }

    public function existe_producto_activado_en_detalle_venta($activaciones, $producto_id){
        foreach($activaciones as $activacion){
            if($activacion->producto_id == $producto_id){
                return $activacion->cantidad;
            }else{
                return null;
            }

        }
    }

    public function obtener_codigos_sin_vender($user_id){
        $total_vendido=\Illuminate\Support\Facades\DB::select("
                                            select count(*) as codigos_vendidos
                                            from activaciones as a
                                            where a.emisor_id=$user_id
                                        ");

        $array_total_comprado=\Illuminate\Support\Facades\DB::select("
                                            select sum(dv.cantidad) as cantidad, v.cliente_id
                                            from ventas as v, detalle_ventas as dv
                                            where v.id=dv.venta_id and v.cliente_id=$user_id
                                            group by cantidad, v.cliente_id
                                        ");
        //dd($array_total_comprado);
        $suma=0;
        foreach($array_total_comprado as $row){
            $suma= $suma + $row->cantidad;
        }
        $total_comprado=$suma;
        return $diferencia=$total_comprado-$total_vendido['0']->codigos_vendidos;

    }
//    public function borrar_codigo_sin_vender(Request $request){
//        // Start transaction
//        DB::beginTransaction();
//        $user_id=$request->user_id;
//        $diferencia=$this->obtener_codigos_sin_vender($user_id);
//        $ventas=Venta::where('cliente_id', '=', $user_id)->get();
//        $acumulador=0;
//        $activacion=DB::select("
//            select count(a.producto_id) as cantidad, a.producto_id, a.emisor_id
//            from activaciones as a
//            where a.emisor_id=$user_id
//            group by a.producto_id, a.emisor_id
//        ");
//        $j=0;
//        foreach ($ventas as $venta){
//
//            $detalle_ventas=DetalleVenta::where('venta_id', '=', $venta->id)->get();
//            $i=0;
//            //dd($detalle_ventas);
//            foreach($detalle_ventas as $det_vent){
//                $acumulador=$acumulador+$det_vent->cantidad;
//                if($diferencia>=$acumulador){
//
//                    //si producto_id se encuentra en $activacion entonces ahi descuento
//                    $cantidad_producto_vendido=$this->existe_producto_activado_en_detalle_venta($activacion, $det_vent->producto_id);
//
//                    if(!isset($cantidad_producto_vendido)){
//                        $dvr = new DetalleVentaRespaldo();
//                        $dvr->detalle_ventas_id=$det_vent->id;
//                        $dvr->cantidad_antigua=$det_vent->cantidad;
//                        $dvr->cantidad_nueva=0;
//                        $dvr->save();
//                        $det_vent->cantidad=0;
//                        $det_vent->save();
//                    }else{
//                        if($cantidad_producto_vendido>$det_vent->cantidad){
//                            $dvr = new DetalleVentaRespaldo();
//                            $dvr->detalle_ventas_id=$det_vent->id;
//                            $dvr->cantidad_antigua=$det_vent->cantidad;
//                            $dvr->cantidad_nueva=0;
//                            $dvr->save();
//                            $det_vent->cantidad=$dvr->cantidad_nueva;
//                            $det_vent->save();
//                        }else{
//                            $dvr = new DetalleVentaRespaldo();
//                            $dvr->detalle_ventas_id=$det_vent->id;
//                            $dvr->cantidad_antigua=$det_vent->cantidad;
//                            $quitar=$det_vent->cantidad-$cantidad_producto_vendido;
//                            $dvr->cantidad_nueva=$det_vent->cantidad-$quitar;
//                            $dvr->save();
//                            $det_vent->cantidad=$dvr->cantidad_nueva;
//                            $det_vent->save();
//                        }
//                    }
//                }else{
////                    if($i===2){
//////                        dd('acumulador: '.$acumulador. ' diferencia:'.$diferencia);
////                        dd('nos pasamos');
////                        //dd('diferencsdffasdfia: '.$diferencia.' Acumulador:'.$acumulador);
////                    }
//                    $dvr = new DetalleVentaRespaldo();
//                    $dvr->detalle_ventas_id=$det_vent->id;
//                    $dvr->cantidad_antigua=$det_vent->cantidad;
//                    $dvr->cantidad_nueva=0;
//                    $dvr->save();
//                    $det_vent->cantidad=0;
//                    $det_vent->save();
//                }
//                $i++;
//            }
//            $j++;
//        }
//
//        if(isset($ventas)){
//            $estado=200;
//            $mensaje='Se recupero fecha de envio';
//        }else{
//            $estado=400;
//            $mensaje='Error al recupera fecha';
//        }
//        DB::rollback();
//        return response()->json([
//            'data' => [
//                'status'  => $estado,
//                'message' => $mensaje,
//                'data'    => $activacion,
//            ],
//        ]);
//    }


    public function historial_codigos(Request $request){
        $user_id=$request->user_id;
        $fecha_inicio=$request->fecha_inicio;
        $fecha_final =$request->fecha_final;
//        $fecha_final ='2019/08/30 15:17';

        $codigos_vendidos=DB::select("
                                          select *
                                                from activaciones as a
                                                where a.emisor_id=$user_id and a.fecha
                                                BETWEEN $fecha_inicio AND $fecha_final
                                            ");
        $total_vendido=DB::select("
                                          select count(*) as total
                                                from activaciones as a
                                                where a.emisor_id=$user_id and a.fecha
                                                BETWEEN $fecha_inicio AND $fecha_final
                                            ");

//        $total_vendido=Activacion::where('fecha', '=', $fecha_final)
//            ->get();

        if(isset($codigos_vendidos)){
            $estado=200;
            $mensaje='Se recupero codigos con exito de envio';
        }else{
            $estado=400;
            $mensaje='Error al recupera fecha';
        }
        return response()->json([
            'data' => [
                'status'  => $estado,
                'message' => $mensaje,
                'data'    => [
                    'codigos_vendidos'  => $codigos_vendidos,
                    'total_vendido'     =>  $total_vendido
                ],
            ],
        ]);
    }


    public function get_tipo_usuario(Request $request){
//        $activacion_id=$request->activacion_id;
//        $activacion=Activacion::find($activacion_id);
//        if(isset($activacion)){
//            $estado=200;
//            $mensaje='Se recupero fecha de envio';
//        }else{
//            $estado=400;
//            $mensaje='Error al recupera fecha';
//        }
        if($request->get('tipo_usuario')==='PREMIUM'){
            $users=DB::select("
 select DISTINCT u.id, u.role_id, u.name, u.email, u.avatar, u.password, u.remember_token,
                                        u.settings, u.created_at, u.updated_at, u.celular, u.validado, u.activo, u.premium, u.google_id,
                                        u.facebook_id, u.Ciudad, u.Municipio, u.Pais, u.TipoDeInformacion, u.TIPO, u.ci, u.direccion,
                                        u.numero_identificacion, u.fecha_inscripcion, u.estado_promotor, u.link, u.departamento_id, u.provincia_id, p.nombre as nombre, m.nombre as nombre_municipio
from users as u, activaciones as a, paises as p, municipios as m
                                        where a.receptor_id=u.id and p.id=u.Pais and u.Municipio=m.id
                        ");
            foreach($users as $user){
                $user->texto_usuario='PREMIUM';
                //dd($user);
            }
        }else{
            $users=DB::select("
                select *
                from vista_usuarios_premium as v
                RIGHT join vista_usuarios_free as u
                on u.id=v.id
                where v.id is null
            ");
            foreach($users as $user){
                $user->texto_usuario='USUARIO';
                //dd($user);
            }

        }
        $total=count($users);

        return response()->json([
            'data' => [
                'status'  => 200,
                'message' => 'Se recupero',
                'data'    => $users,
                'total'    => $total,
            ],
        ]);
    }


    public function get_fecha_envio_codigo(Request $request){
        $activacion_id=$request->activacion_id;
        $activacion=Activacion::find($activacion_id);
        if(isset($activacion)){
            $estado=200;
            $mensaje='Se recupero fecha de envio';
        }else{
            $estado=400;
            $mensaje='Error al recupera fecha';
        }
        return response()->json([
            'data' => [
                'status'  => $estado,
                'message' => $mensaje,
                'data'    => $activacion,
            ],
        ]);
    }

    public function loginGoogle(Request $request){

        $user = DB::table('users as u')
          ->select('u.id','u.name','u.email','u.avatar','premium','u.celular')
          ->where('email', $request->email)
          ->first();
        $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
        if(!$user){
            $new_user = new User();
            $new_user->email = $request->email;
            $new_user->name = $request->name;
            $new_user->google_id = $request->google_id;
            $new_user->role_id = 2;
            $new_user->premium = 'n';
            $new_user->save();

            $user = DB::table('users as u')
            ->select('u.id','u.name','u.email','u.avatar','u.premium','u.celular')
            ->where('u.id', $new_user->id)
            ->first();
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'registro y login con Google realizado correctamente',
                'datos' => $user
             ],200);
         }else{
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'login realizado correctamente',
                'datos' => $user
            ],200);
        }
    }
  
     public function loginFacebook(Request $request){
        $user = DB::table('users as u')
          ->select('u.id','u.name','u.email','u.avatar','premium','u.celular')
          ->where('email', $request->email)
          ->first();
        $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
        if(!$user){
            $new_user = new User();
            $new_user->email = $request->email;
            $new_user->name = $request->name;
            $new_user->facebook_id = $request->facebook_id;
            $new_user->role_id = 2;
            $new_user->premium = 'n';
            $new_user->save();

            $user = DB::table('users as u')
            ->select('u.id','u.name','u.email','u.avatar','u.premium','u.celular')
            ->where('u.id', $new_user->id)
            ->first();
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'registro y login con Facebook realizado correctamente',
                'datos' => $user
             ],200);
         }else{
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'login realizado correctamente',
                'datos' => $user
            ],200);
        }
    }


    public function registrarCelular(Request $request,$user_id){
        $user = DB::table('users as u')
          ->where('celular', $request->celular)
          ->first();
        $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
        if(!$user){
            //celular disponible
            $user = User::find($user_id);
//            dd('this is a dd'); 
//           dd($user);
            $user->celular = $request->celular;
            $user->save();
          
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'registro del celular realizado correctamente',
                'datos' => $user,
                'codigo' => 0
             ],200);
         }else{
            //celular ya ocupado
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'no se pudo registrar, el celular ya esta en uso',
                'datos' => $user,
                'codigo' => 1
            ],200);
        }
    }
  
  

  
 


}
