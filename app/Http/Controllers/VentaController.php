<?php

namespace App\Http\Controllers;

use App\Pais;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Alert;
use App\User;
use App\Producto;
use Carbon\Carbon;
use App\Venta;
use App\DetalleVenta;
use App\Stock;
use App\Promotor;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;


class VentaController extends Controller
{
    use BreadRelationshipParser;

   public $fecha;
   public function __construct (){
      $this->fecha = Carbon::now()->format('Y-m-d H:i:s');
  }
  
    public function ventaForm($user_id){
        $cliente = User::find($user_id);
        $productos = DB::table('productos as p')
            ->where('p.estado',1)
            ->get();
        $fecha = Carbon::now()->toDateString();
        return view('activaciones.venta-form', ['cliente'=>$cliente,'productos'=>$productos,'fecha'=>$fecha]);
    }

      public function ventaCodigos(Request $request,$cliente_id){  
        DB::beginTransaction();
            try {
                $venta = new Venta();
                $venta->vendedor_id = auth()->user()->id;
                $venta->cliente_id = $cliente_id;
                $venta->total = $request->total;
                $venta->fecha = $this->fecha;
                $venta->estado = 'realizada';
                $venta->save();
              //dd($request->all());
                foreach($request->table as $row){
                  $detalle_venta = new DetalleVenta();
                  $detalle_venta->cantidad = $row['cantidad'];
                  $detalle_venta->precio_actual = $row['precio'];
                  $detalle_venta->subtotal = $row['subtotal'];
                  $detalle_venta->venta_id = $venta->id;
                  $detalle_venta->producto_id = $row['id'];
                  $detalle_venta->save();
                  
                  $stock = new Stock();
                  $stock->glosa = 'ingreso';
                  $stock->cantidad = $row['cantidad'];
                  $stock->fecha = $this->fecha;
                  $stock->user_id = $cliente_id;
                  $stock->producto_id = $row['id'];
                  $stock->save();
                }
                DB::commit();
//                 Alert::success('venta realizada correctamente!','venta de codigos');
//                 return redirect()->route('welcome');
              return redirect()->back();
                
            } catch (\Exception $e){
                DB::rollBack();
//                 Alert::error('ocurrio un error','venta de codigos');
//                 return redirect()->route('ver-ventas');
                return redirect()->back();
            }
    }
  
   public function verDetalle($venta_id){
       $venta = DB::table('ventas as v')
           ->select('v.total','v.id','v.total','v.fecha','u.name as cliente')
           ->join('users as u','u.id','v.cliente_id')
           ->where('v.id',$venta_id)
           ->first();

       $detalles = DB::table('detalle_ventas as dv')
           ->select('p.nombre as producto','dv.cantidad','dv.precio_actual','dv.subtotal')
           ->join('productos as p','p.id','dv.producto_id')
           ->where('dv.venta_id',$venta_id)
           ->get();
       return view('detalle-ventas',['venta'=>$venta,'detalles'=>$detalles]);
   }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
        $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
        $orderBy = $request->get('order_by');
        $sortOrder = $request->get('sort_order', null);

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $relationships = $this->getRelationships($dataType);

            $model = app($dataType->model_name);
            $query = $model::select('*')->with($relationships);

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'DESC';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        $view = "voyager::$slug.reporte.browse";
        $dataTypeContent = DB::select("
        select sum(v.total) as total, p.nombre as pais, d.nombre as departamento
FROM ventas as v, users as u, paises as p, departamentos as d
where v.cliente_id=u.id and u.Pais=p.id and u.departamento_id=d.id
group by p.nombre, d.nombre
        ");

        //dd($ventas);
        $paises=Pais::all();

        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'sortOrder',
            'searchable',
            'isServerSide',
            'paises'
        ));
    }


}






