<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;

class ApiLoginController extends Controller
{  
    public function loginGoogle(Request $request){

        $user = DB::table('users as u')
          ->select('u.id','u.name','u.email','u.avatar','premium','u.celular')
          ->where('email', $request->email)
          ->first();
        $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
        if(!$user){
            $new_user = new User();
            $new_user->email = $request->email;
            $new_user->name = $request->name;
            $new_user->google_id = $request->google_id;
            $new_user->role_id = 2;
            $new_user->premium = 'n';
            $new_user->save();

            $user = DB::table('users as u')
            ->select('u.id','u.name','u.email','u.avatar','u.premium','u.celular')
            ->where('u.id', $new_user->id)
            ->first();
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'registro y login con Google realizado correctamente',
                'datos' => $user
             ],200);
         }else{
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'login realizado correctamente',
                'datos' => $user
            ],200);
        }
    }
  
     public function loginFacebook(Request $request){
        $user = DB::table('users as u')
          ->select('u.id','u.name','u.email','u.avatar','premium','u.celular')
          ->where('email', $request->email)
          ->first();
        $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
        if(!$user){
            $new_user = new User();
            $new_user->email = $request->email;
            $new_user->name = $request->name;
            $new_user->facebook_id = $request->facebook_id;
            $new_user->role_id = 2;
            $new_user->premium = 'n';
            $new_user->save();

            $user = DB::table('users as u')
            ->select('u.id','u.name','u.email','u.avatar','u.premium','u.celular')
            ->where('u.id', $new_user->id)
            ->first();
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'registro y login con Facebook realizado correctamente',
                'datos' => $user
             ],200);
         }else{
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'login realizado correctamente',
                'datos' => $user
            ],200);
        }
    }


    public function registrarCelular(Request $request,$user_id){
        $user = DB::table('users as u')
          ->where('celular', $request->celular)
          ->first();
        $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
        if(!$user){
            //celular disponible
            $user = User::find($user_id);
//            dd('this is a dd'); 
//           dd($user);
            $user->celular = $request->celular;
            $user->save();
          
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'registro del celular realizado correctamente',
                'datos' => $user,
                'codigo' => 0
             ],200);
         }else{
            //celular ya ocupado
            return response()->json([
                'fecha' => $fecha,
                'mensaje' => 'no se pudo registrar, el celular ya esta en uso',
                'datos' => $user,
                'codigo' => 1
            ],200);
        }
    }
  
  

  
 


}
