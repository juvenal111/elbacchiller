<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class PsaAreaController extends Controller
{

  public function getPsaAreas($psa_gestion_id){
      $areas = DB::table('psa_areas as a')
        ->select('a.id','a.nombre')
        ->where('a.psa_gestion_id',$psa_gestion_id)
        ->where('a.estado',1)
        ->get();
      $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
      if($areas){
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'lista de areas del PSA de la gestion con ID:'.$psa_gestion_id,
              'datos' => $areas
          ],200);
      }else{
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'no hay areas del PSA registradas de la gestion con ID:'.$psa_gestion_id,
              'datos' => null
          ],404);
      }
  }
  
}
