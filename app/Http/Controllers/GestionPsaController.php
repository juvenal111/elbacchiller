<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\GestionPsa;


class GestionPsaController extends Controller
{

    public function verGestionesPsa($psa_id)
    {
        $gestiones  =  DB::table('gestiones_psa as g')
            ->where('g.psa_id',$psa_id)
            ->get();

        $psa = DB::table('psas as p')
            ->select('p.id','c.nombre as nombre_ciudad')
            ->join('ciudades as c','c.id','p.ciudad_id')
            ->where('p.id',$psa_id)
            ->first();
        return view('psa2.gestiones_psa.index',['gestiones' => $gestiones , 'psa' => $psa]);
    }

    public function guardarGestionPsa(Request $request,$psa_id)
    {
        $gestion = new GestionPsa();
        $gestion->nombre = $request->nombre;
        $gestion->psa_id = $psa_id;
        $gestion->save();
        return redirect()->back();
    }

    public function editarGestionPsa(Request $request,$gestion_id){
        $gestion = GestionPsa::find($gestion_id);
        $gestion->nombre = $request->nombre;
        $gestion->save();
        return redirect()->back();
    }

    public function eliminarGestionPsa($gestion_id)
    {
        $gestion = GestionPsa::find($gestion_id);
        $gestion->delete();
        return redirect()->back();
    }

}
