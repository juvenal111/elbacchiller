<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\PsaPregunta;
use App\PsaRespuesta;
use App\PsaAreaMateria;
use Illuminate\Support\Facades\Storage;

class PsaGestionController extends Controller
{
  
  public function getPsaGestiones($ciudades_id){
      $gestiones = DB::table('psa_gestiones as g')
        ->select('g.id','g.nombre')
        ->where('g.estado',1)
        ->where('ciudades_id',$ciudades_id)
        ->get();
      $fecha = Carbon::now('America/La_Paz')->format('d-m-Y H:i:s');
      if($gestiones){
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'lista de gestiones del PSA',
              'datos' => $gestiones
          ],200);
      }else{
          return response()->json([
              'fecha' => $fecha,
              'mensaje' => 'no hay gestiones del PSA registradas',
              'datos' => null
          ],404);
      }
  }
  
  
}
