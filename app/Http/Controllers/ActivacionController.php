<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Stock;
use App\Activacion;
use Carbon\Carbon;


class ActivacionController extends Controller
{
  public $fecha;
  
  public function __construct (){
      $this->fecha = Carbon::now()->format('Y-m-d H:i:s');
  }
  
  public function getStock( $user_id ){
        $user = DB::table('users as u')->where('u.id',$user_id)->first();
        if(!$user){ //usuario invalido
               return response()->json([
                'codigo' => 404,
                'mensaje' => 'usuario '.$user_id.' invalido',
                'datos' => null,
                'fecha' => $this->fecha
            ],404);
        }
        $stock = DB::select('
            SELECT p.id ,p.nombre,SUM(s.cantidad) as stock
            FROM stocks as s, productos as p
            WHERE p.id = s.producto_id AND s.user_id='.$user_id.'
            GROUP BY p.id,p.nombre
        ');
        if(!$stock){  // no tiene stock
            return response()->json([
                'codigo' => 200,
                'mensaje' => 'usuario '.$user_id.' no tiene stock de activaciones',
                'datos' => null,
                'fecha' => $this->fecha
            ],200);
        }else{
            return response()->json([
                'codigo' => 200,
                'mensaje' => 'stock de activaciones del usuario '.$user_id,
                'datos' => $stock,
                'fecha' => $this->fecha
            ],200);
        }
    }

    public function transferirActivacion( $user_id, $producto_id, Request $request ){
        $user = DB::table('users as u')->where('u.id',$user_id)->first();
        if(!$user){ //usuario invalido
               return response()->json([
                'codigo' => 404,
                'mensaje' => 'usuario '.$user_id.' invalido',
                'datos' => null,
                'fecha' => $this->fecha,
            ],404);
        }
        $producto = DB::table('productos as p')->where('p.id',$producto_id)->first();
        if(!$producto){ // producto invalido
               return response()->json([
                'codigo' => 404,
                'mensaje' => 'producto '.$producto_id.' invalido',
                'datos' => null,
                'fecha' => $this->fecha
            ],404);
        }
        $receptor = DB::table('users as u')
            ->where('u.celular',$request->celular)
            ->first();
        if(!$receptor){ // celular invalido
            return response()->json([
                'codigo' => 404,
                'mensaje' => 'celular '.$request->celular.' invalido',
                'datos' => null,
                'fecha' => $this->fecha
            ],404);
        }else{ 
            DB::beginTransaction();
            try {
                $activacion = new Activacion();
                $activacion->estado = 'enviada';
                $activacion->fecha= $this->fecha;
                $activacion->emisor_id = $user_id;
                $activacion->receptor_id = $receptor->id;
                $activacion->producto_id = $producto_id;
                $activacion->save();
              
                $stock = new Stock();
                $stock->glosa = 'egreso';
                $stock->cantidad = -1;
                $stock->fecha = $this->fecha;
                $stock->user_id = $user_id;
                $stock->producto_id = $producto_id;
                $stock->save();
                DB::commit();
            } catch (\Exception $e){
                DB::rollBack();
                return response()->json([
                    'codigo' => 500,
                    'mensaje' => 'error durante transaccion (rollback)',
                    'datos' => null,
                    'fecha' => $this->fecha
                ],500);
            }
            return response()->json([
                'codigo' => 200,
                'mensaje' => 'transferencia realizada con exito',
                'datos' => $activacion,
                'fecha' => $this->fecha
            ],200);
        }
    }
  
  public function getActivacion( $user_id ){
       $user = DB::table('users as u')->where('u.id',$user_id)->first();
        if(!$user){ //usuario invalido
               return response()->json([
                'codigo' => 404,
                'mensaje' => 'usuario '.$user_id.' invalido',
                'datos' => null,
                'fecha' => $this->fecha
            ],404);
        }
      $activacion = DB::table('activaciones as a')
         ->select('a.id','a.estado','a.fecha_activacion','a.fecha_expiracion','p.nombre as producto','u.name as emisor')
         ->join('productos as p','p.id','a.producto_id')
         ->join('users as u','u.id','a.emisor_id')
         ->where('a.receptor_id',$user_id)
         ->where('a.estado','enviada')
         ->first();
        if(!$activacion){
             return response()->json([
                'codigo' => 200,
                'mensaje' => 'usuario '.$user_id.' no tiene ninguna activacion disponible',
                'datos' => null,
                'fecha' => $this->fecha
            ],200);
        }else{
             return response()->json([
                'codigo' => 200,
                'mensaje' => 'actiacion disponible',
                'datos' => $activacion,
                'fecha' => $this->fecha
            ],200);   
        }
  }
  
   public function activar( $activacion_id ){
       $activacion = DB::table('activaciones as a')->where('a.id',$activacion_id)->first();
        if(!$activacion){ //activacion invalida
               return response()->json([
                'codigo' => 404,
                'mensaje' => 'activacion '.$activacion_id.' invalida',
                'datos' => null,
                'fecha' => $this->fecha
            ],404);
        }
        $activacion = Activacion::find($activacion_id);
     
        if($activacion->estado == 'activada'){  //activacion ya esta activada
            return response()->json([
              'codigo' => 200,
              'mensaje' => 'activacion ya esta activada',
              'datos' => $activacion,
              'fecha' => $this->fecha
          ],200);
        }else{ // activar
            $producto = DB::table('productos as p')
              ->select('p.dias_duracion')
              ->join('activaciones as a','a.producto_id','p.id')
              ->where('a.id',$activacion_id)
              ->first();
            $fecha_expiracion = Carbon::now('America/La_Paz')->addDays($producto->dias_duracion)->format('Y-m-d H:i:s');
            $activacion->fecha_activacion =  $this->fecha;
            $activacion->fecha_expiracion =  $fecha_expiracion;
            $activacion->estado = 'activada';
            $activacion->save();
          
              return response()->json([
                  'codigo' => 200,
                  'mensaje' => 'activacion realizada correctamente',
                  'datos' => $activacion,
                  'fecha' => $this->fecha
              ],200);
        }
    }

    public function getHistorial($user_id){
        $user = DB::table('users as u')->where('u.id',$user_id)->first();
        if(!$user){ //usuario invalido
               return response()->json([
                'codigo' => 404,
                'mensaje' => 'usuario '.$user_id.' invalido',
                'datos' => null,
                'fecha' => $this->fecha
            ],404);
        }
        $activaciones = DB::table('activaciones as a')
            ->where('a.emisor_id',$user_id)
            ->get();
        if($activaciones->isEmpty()){
            return response()->json([
                'codigo' => 200,
                'mensaje' => 'historial vacio del usuario '.$user_id,
                'datos' => null,
                'fecha' => $this->fecha
            ],200);
        }else{
            return response()->json([
                'codigo' => 200,
                'mensaje' => 'historial de envio de activaciones del usuario '.$user_id,
                'datos' => $activaciones,
                'fecha' => $this->fecha
            ],200);          
        }
    }
  

}
