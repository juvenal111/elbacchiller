<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\PsaAreaMateria;
use Alert;

class PsaAreaMateriaController extends Controller
{
  
  public function verAreaMaterias($area_id){
    $search = \Request::get('search');  
    $materias = DB::table('psa_area_materias as am')
        ->select('m.id','m.nombre')
        ->join('psa_materias as m','m.id','am.psa_materia_id')
        ->where('am.psa_area_id',$area_id)        
        ->where('m.nombre','like','%'.$search.'%')
        ->orderBy('m.id','desc')
        ->paginate(5);
      
    $area = DB::table('psa_areas as a')
        ->select('a.id','a.nombre','g.nombre as nombre_gestion')
        ->join('psa_gestiones as g','g.id','a.psa_gestion_id')
        ->where('a.id',$area_id)
        ->first();
    return view('psa.materias.index',['materias'=>$materias,'area'=> $area]);  
  }

    public function editarAreaMateria(Request $request,$area_materia_id){
      $area_materia = PsaAreaMateria::find($area_materia_id);
      $area_materia->link_texto = $request->link_texto;
      $area_materia->save();
//       Alert::success('AreaMateria actualizada correctamente','AreaMateria');
//       return redirect()->route('ver-preguntas',['area_id'=>$area_materia->psa_area_id ,'materia_id'=>$area_materia->psa_materia_id]);
      return redirect()->back();
  }

  
  
  
}
