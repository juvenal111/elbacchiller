<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PsaAreaMateria extends Model
{
    protected $table = 'psa_area_materias';
}
