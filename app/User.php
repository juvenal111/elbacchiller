<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use TCG\Voyager\Facades\Voyager;


class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function activaciones(){
        return $this->hasMany('App\Activacion','receptor_id');
    }
    public function role(){
        $roleModel = Voyager::modelClass('Role');
        return $this->belongsTo($roleModel,'role_id');
    }
    public function pais(){
        $roleModel = 'App\Pais';
        return $this->belongsTo($roleModel,'Pais');
    }


}
