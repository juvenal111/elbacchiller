<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaMateriasPsaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_materias_psa', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area_psa_id')->unsigned();
                    $table->foreign('area_psa_id')->references('id')->on('areas_psa');
            $table->integer('materia_psa_id')->unsigned();
                    $table->foreign('materia_psa_id')->references('id')->on('materias_psa');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_materias_psa');
    }
}
