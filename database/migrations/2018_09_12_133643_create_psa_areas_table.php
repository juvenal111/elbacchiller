<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePsaAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('psa_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',255);
            $table->string('estado',1)->nullable();
            $table->integer('psa_gestion_id')->unsigned();
                $table->foreign('psa_gestion_id')->references('id')->on('psa_gestiones');   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('psa_areas');
    }
}
