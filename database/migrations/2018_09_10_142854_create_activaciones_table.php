<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('estado',255)->nullable(); 
            $table->datetime('fecha')->nullable(); 
            $table->datetime('fecha_activacion')->nullable(); 
            $table->datetime('fecha_expiracion')->nullable(); 
            $table->integer('emisor_id')->unsigned();
                $table->foreign('emisor_id')->references('id')->on('users');
            $table->integer('receptor_id')->unsigned();
                $table->foreign('receptor_id')->references('id')->on('users');
            $table->integer('producto_id')->unsigned();
                $table->foreign('producto_id')->references('id')->on('productos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activaciones');
    }
}
