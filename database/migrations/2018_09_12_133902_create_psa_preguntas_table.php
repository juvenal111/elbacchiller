<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePsaPreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('psa_preguntas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('texto',5000);
            $table->string('imagen',255)->nullable();
            $table->string('estado',1)->nullable();
            $table->integer('psa_area_materias_id')->unsigned();
                $table->foreign('psa_area_materias_id')->references('id')->on('psa_area_materias');
            $table->string('texto2',5000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('psa_preguntas');
    }
}
