<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreguntasPsaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preguntas_psa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('texto',5000);
            $table->string('texto2',5000)->nullable();
            $table->string('imagen',255)->nullable();
            $table->string('estado',1)->nullable();
            $table->integer('materia_psa_id')->unsigned();
                    $table->foreign('materia_psa_id')->references('id')->on('materias_psa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preguntas_psa');
    }
}
