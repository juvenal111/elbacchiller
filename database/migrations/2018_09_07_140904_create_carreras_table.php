<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carreras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',255);
            $table->string('modalidad',255);
            $table->string('lugar',255)->nullable();
            
            $table->string('grado_academico',255)->nullable();
            $table->string('regimen_de_estudio',255)->nullable();
            $table->string('duracion',255)->nullable();
            $table->string('inicio_de_clases',255)->nullable();
          
            $table->string('costo_inscripcion',255)->nullable();
            $table->string('costo_matricula',255)->nullable();

            $table->string('horario',255)->nullable();
            
            $table->string('estado',1)->nullable();
            $table->integer('institucion_id')->unsigned();
                $table->foreign('institucion_id')->references('id')->on('instituciones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carreras');
    }
}
