<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGestionesPsaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestiones_psa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',255);
            $table->string('estado',1)->nullable();
            $table->integer('psa_id')->unsigned();
                    $table->foreign('psa_id')->references('id')->on('psas');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gestiones_psa');
    }
}
