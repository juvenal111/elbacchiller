<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuntoComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('punto_compras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',255)->nullable();
            $table->string('link_maps',255)->nullable();
            $table->string('imagen',255)->nullable();
            $table->string('imagen2',255)->nullable();
            $table->string('imagen3',255)->nullable();

            $table->float('lat',8,6)->nullable();
            $table->float('lng', 8, 6)->nullable();
            $table->string('estado',1)->nullable();
            $table->integer('ciudad_id')->unsigned();
             $table->foreign('ciudad_id')->references('id')->on('ciudades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('punto_compras');
    }
}
