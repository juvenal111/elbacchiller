<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilCarrerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfil_carreras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',255);
            $table->string('imagen',255)->nullable();
            $table->string('descripcion',5000)->nullable();
            $table->string('perfil_ocupacional',5000)->nullable();
            $table->string('estado',1)->nullable();
            $table->integer('area_id')->unsigned();
                $table->foreign('area_id')->references('id')->on('areas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfil_carreras');
    }
}
