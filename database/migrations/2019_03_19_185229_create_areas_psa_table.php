<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasPsaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas_psa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',255);
            $table->string('estado',1)->nullable();
            $table->integer('gestion_psa_id')->unsigned();
                    $table->foreign('gestion_psa_id')->references('id')->on('gestiones_psa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas_psa');
    }
}
