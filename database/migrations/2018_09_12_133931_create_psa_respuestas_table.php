<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePsaRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('psa_respuestas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('texto',5000);
            $table->string('imagen',255)->nullable();
            $table->string('tipo',255)->nullable();
            $table->string('estado',1)->nullable();
            $table->integer('psa_pregunta_id')->unsigned();
                $table->foreign('psa_pregunta_id')->references('id')->on('psa_preguntas');   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('psa_respuestas');
    }
}
