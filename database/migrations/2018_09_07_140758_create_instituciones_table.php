<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitucionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instituciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',255);
            $table->string('logo',255)->nullable();
            $table->string('portada',255)->nullable();
            $table->string('mapeo_interno',255)->nullable();
            $table->float('lat',8,6)->nullable();
            $table->float('lng', 8, 6)->nullable();
            $table->string('gestion',255)->nullable();
            $table->date('fecha_actualizacion')->nullable();
            $table->string('fecha_inscripcion',255)->nullable();
            $table->date('fecha_inicio_clases')->nullable();
            $table->string('beneficios',5000)->nullable();
            $table->string('requisitos',5000)->nullable();
            $table->string('ubicacion',255)->nullable();
            $table->string('telefono',255)->nullable();
            $table->string('correo',255)->nullable();
            $table->string('link_web',255)->nullable();
            $table->string('link_maps',255)->nullable();
            $table->string('link_facebook',255)->nullable();    
            $table->string('link_twitter',255)->nullable();
            $table->string('link_youtube',255)->nullable();
            $table->string('tipo',255)->nullable();
            $table->string('estado',1)->nullable();
            $table->integer('ciudad_id')->unsigned();
                $table->foreign('ciudad_id')->references('id')->on('ciudades');
            $table->string('provincia',255)->nullable();
            $table->string('municipio',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instituciones');
    }
}
