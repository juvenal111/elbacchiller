<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePsaAreaMateriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('psa_area_materias', function (Blueprint $table) {
          $table->increments('id');
          $table->string('link_texto',255)->nullable();
          $table->integer('psa_area_id')->unsigned();
                $table->foreign('psa_area_id')->references('id')->on('psa_areas');   
          $table->integer('psa_materia_id')->unsigned();
                $table->foreign('psa_materia_id')->references('id')->on('psa_materias');   
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('psa_area_materias');
    }
}
