<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestasPsaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas_psa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('texto',5000);
            $table->string('imagen',255)->nullable();
            $table->string('tipo',255)->nullable();
            $table->string('estado',1)->nullable();
            $table->integer('pregunta_psa_id')->unsigned();
                    $table->foreign('pregunta_psa_id')->references('id')->on('preguntas_psa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuestas_psa');
    }
}
