<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('total',12,2)->nullable();
            $table->datetime('fecha')->nullable();      
            $table->string('estado',255)->nullable();      
            $table->integer('vendedor_id')->unsigned();
                $table->foreign('vendedor_id')->references('id')->on('users');
            $table->integer('cliente_id')->unsigned();
                $table->foreign('cliente_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
