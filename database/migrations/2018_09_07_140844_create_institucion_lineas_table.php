<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitucionLineasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institucion_lineas', function (Blueprint $table) {
            $table->integer('institucion_id')->unsigned();
                $table->foreign('institucion_id')->references('id')->on('instituciones');
            $table->integer('linea_id')->unsigned();
                $table->foreign('linea_id')->references('id')->on('lineas');
            $table->primary(['institucion_id', 'linea_id']);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institucion_lineas');
    }
}
