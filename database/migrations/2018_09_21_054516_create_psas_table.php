<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePsasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('psas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gestion',255);
            $table->string('imagen1',255)->nullable();
            $table->string('imagen2',255)->nullable();
            $table->text('descripcion1')->nullable();
            $table->text('descripcion2')->nullable();
            $table->text('procedimientos')->nullable();
            $table->text('requisitos')->nullable();
            $table->text('ponderaciones')->nullable();
            $table->text('carreras_ofertadas')->nullable();
            $table->integer('ciudad_id')->unsigned();
                $table->foreign('ciudad_id')->references('id')->on('ciudades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('psas');
    }
}
